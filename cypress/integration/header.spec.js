describe('Header.js (It displays the menu)', function() {
  it('Menu when there is not current user', function() {
    cy.visit('/');

    cy.get('label > .header__menu > li').should(($menu) => {
      expect($menu).to.have.length(6);
      expect($menu.eq(1)).to.contain('Tips');
      expect($menu.eq(2)).to.contain('Challenges');
      expect($menu.eq(3)).to.contain('Carbon Footprint Calculator');
      expect($menu.eq(4)).to.contain('Contact');
      expect($menu.eq(5)).to.contain('Downloads');
    });

    cy.get('label > .header__menu > .header__menu > li').should(($menu) => {
      expect($menu).to.have.length(2);
      expect($menu.eq(0)).to.contain('Sign in');
      expect($menu.eq(1)).to.contain('Sign up');
    });
  });

  it('Menu when there is current user', function() {
    // Logging In with an existing user
    const data = {
      email: 'hiber98@gmail.com',
      password: 'hiber98',
    };

    cy.visit('/login');

    cy.get('input[id=email]').type(data.email);
    cy.get('input[id=password]').type(`${data.password}{enter}`);

    // Waiting to Log In.
    cy.wait(3000);

    cy.get('label > .header__menu > li').should(($menu) => {
      expect($menu).to.have.length(6);
      expect($menu.eq(1)).to.contain('Tips');
      expect($menu.eq(2)).to.contain('Challenges');
      expect($menu.eq(3)).to.contain('Carbon Footprint Calculator');
      expect($menu.eq(4)).to.contain('Contact');
      expect($menu.eq(5)).to.contain('Downloads');
    });

    cy.get('label > .header__menu > .header__menu > li').should(($menu) => {
      expect($menu).to.have.length(2);
      expect($menu.eq(0)).to.contain('hiber98');
      expect($menu.eq(1)).to.contain('Log Out');
    });
  });
});
