describe('The Tips Page', function() {
  it('It checks the Main view', function() {
    // visit 'tips'
    cy.visit('/tips');

    // check if url have changed
    cy.url().should('includes', 'tips');

    cy.get('.container__name').should('have.text', 'Tips');
  });

  it('Checks the default products, categories and the pagination', function() {
    // visit 'tips'
    cy.visit('/tips');

    // Check how many categories there are
    cy.get('.categories > .categories__category').should('have.length', 5);

    // Check if there are 2 tabs. The global and the categorie's name
    cy.get('.container__links > *').should('have.length', 1);

    // Check how many tips there are
    cy.get('.news__items > .news').should('have.length', 10);

    // Check if there are 2 pages.
    cy.get('.pagination > .page__item > .page__number').should(($pages) => {
      expect($pages).to.have.length(2);
      expect($pages.eq(0)).to.contain('1');
      expect($pages.eq(1)).to.contain('2');
    });
  });

  it('Tips get filtered by clicking on a category', function() {
    // visit 'tips'
    cy.visit('/tips');

    // Click on the rem tab
    cy.contains('rem').click();

    // Check if url has changed
    cy.url().should('includes', 'tips?tab=category&category=rem');

    // Check if there are 2 tabs. The global and the categorie's name
    cy.get('.container__links > *').should('have.length', 2);

    // Check if the number of categories has changed
    cy.get('.news__items > .news').should('have.length', 0);

    // Check if the number of pages has changed
    cy.get('.pagination > .page__item > .page__number')
        .should('have.length', 0);

    // Click on Global Feed tab to list all tips.
    cy.contains('Global Feed').click();

    // Check if there are 2 tabs. The global and the categorie's name
    cy.get('.container__links > *').should('have.length', 1);

    // Check how many tips there are
    cy.get('.news__items > .news').should('have.length', 10);
  });


  it('Tips get filtered by the current user username', function() {
    // Logging In with an existing user
    const data = {
      email: 'hiber98@gmail.com',
      password: 'hiber98',
    };

    cy.visit('/login');

    cy.get('input[id=email]').type(data.email);
    cy.get('input[id=password]').type(`${data.password}{enter}`);

    // Waiting to Log In.
    cy.wait(3000);

    // visit 'tips'
    cy.visit('/tips');

    // Click on the rem tab
    cy.contains('Your Feed').click();

    // Check if url has changed
    cy.url().should('includes', 'tips?tab=feed');

    // Check if there are 2 tabs. The global and the categorie's name
    cy.get('.container__links > *').should('have.length', 2);

    // Check if the number of categories has changed
    cy.get('.news__items > .news').should('have.length', 5);

    // Check if the number of pages has changed
    cy.get('.pagination > .page__item > .page__number')
        .should('have.length', 2);

    // Click on Global Feed tab to list all tips.
    cy.contains('Global Feed').click();

    // Check if there are 2 tabs. The global and the categorie's name
    cy.get('.container__links > *').should('have.length', 2);

    // Check how many tips there are
    cy.get('.news__items > .news').should('have.length', 10);

    // Check if the number of pages has changed
    cy.get('.pagination > .page__item > .page__number')
        .should('have.length', 2);
  });

  it('Check if pagination works', function() {
    // visit 'tips'
    cy.visit('/tips');

    // Check how many tips there are
    cy.get('.news__items > .news').should('have.length', 10);

    // Click on the rem tab
    cy.get('.pagination > .page__item > .page__number').contains('2').click();

    // Check how many tips there are
    cy.get('.news__items > .news').should('have.length', 2);

    // Click on the rem tab
    cy.get('.pagination > .page__item > .page__number').contains('1').click();

    // Check how many tips there are
    cy.get('.news__items > .news').should('have.length', 10);
  });
});
