describe('The Downloads Page', function() {
  it('Checks the view', function() {
    // visit 'downloads'
    cy.visit('/downloads');

    // check if url have changed
    cy.url().should('includes', 'downloads');

    cy.get('.container__name').should('have.text', 'Downloads');

    // Check how many downloads there are and their names.
    cy.get('.container--downloads > .download > .download__name')
        .should(($menu) => {
          expect($menu).to.have.length(2);
          expect($menu.eq(0)).to.contain('Linux');
          expect($menu.eq(1)).to.contain('Windows');
        });
  });
});
