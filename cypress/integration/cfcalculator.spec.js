describe('The Carbon Footprint Calculator Page', function() {
  it('It checks the Main view', function() {
    // visit 'cfcalc'
    cy.visit('/cfcalc');

    // check if url have changed
    cy.url().should('includes', 'cfcalc');

    cy.get('.container__name')
        .should('have.text', 'Carbon Footprint Calculator');
  });

  it('Checks the default calculator data, and fact', function() {
    // visit 'cfcalc'
    cy.visit('/cfcalc');

    // Check that all the calc fields are equal to 0
    cy.get('.calculator > .calculator__field > .calculator__input')
        .should(($calc) => {
          expect($calc).to.have.length(5);
          expect($calc.eq(0)).to.have.value('0');
          expect($calc.eq(1)).to.have.value('0');
          expect($calc.eq(2)).to.have.value('0');
          expect($calc.eq(3)).to.have.value('0');
          expect($calc.eq(4)).to.have.value('0');
        });

    // Check that result should be 0
    expect(cy.get('.calculator__result--input').should('have.value', '0'));

    // Check for the slides and cheks if
    // the first slide to have the text expected
    cy.get('.slider__container > .slider > .slider__slide').should(($slide) => {
      expect($slide).to.have.length(5);
      // eslint-disable-next-line max-len
      expect($slide.eq(0)).to.have.text('Did you know?14 kg of CO2 emitted are the equivalent of:A car trip of 70 km.Five hamburgers with cheese.Nine liters of milk.6.6 minutes of a transatlantic flight.Reading a newspaper can consume 20% less carbon than watching the news online.Swedish Royal Institute for Technology, Moberg et al, 2007.'
      );
    });
  });

  it('Fills the calc and expect the result is correct', function() {
    // Default data.
    const calcData = {
      param1: '1',
      param2: '1',
      param3: '1',
      param4: '1',
      param5: '1',
    };

    // Fills the calc inputs with the data provided.
    cy.get('.calculator > .calculator__field > .calculator__input').eq(0)
        .clear()
        .type(calcData.param1);
    cy.get('.calculator > .calculator__field > .calculator__input').eq(1)
        .clear()
        .type(calcData.param2);
    cy.get('.calculator > .calculator__field > .calculator__input').eq(2)
        .clear()
        .type(calcData.param3);
    cy.get('.calculator > .calculator__field > .calculator__input').eq(3)
        .clear()
        .type(calcData.param4);
    cy.get('.calculator > .calculator__field > .calculator__input').eq(4)
        .clear()
        .type(calcData.param5);

    // Click on CALCULATE button.
    cy.contains('CALCULATE').click();

    // Expect that the result has changed.
    expect(cy.get('.calculator__result--input').should('have.value', '17.1'));
  });
});
