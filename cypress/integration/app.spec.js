describe('App.js (It contains all the routes)', function() {
  it('Routes that wotks with auth and non auth users', function() {
    // visit 'contact'
    cy.visit('/contact');
    // Check if url has changed
    cy.url().should('includes', 'contact');
    // Check if the component title has changed.
    cy.get('.container__name').should('have.text', 'Contact Form');

    // visit 'tips'
    cy.visit('/tips');
    // Check if url has changed
    cy.url().should('includes', 'tips');
    // Check if the component title has changed.
    cy.get('.container__name').should('have.text', 'Tips');

    // visit 'challenges'
    cy.visit('/challenges');
    // Check if url has changed
    cy.url().should('includes', 'challenges');
    // Check if the component title has changed.
    cy.get('.container__name').should('have.text', 'Challenges');

    // visit 'downloads'
    cy.visit('/downloads');
    // Check if url has changed
    cy.url().should('includes', 'downloads');
    // Check if the component title has changed.
    cy.get('.container__name').should('have.text', 'Downloads');

    // visit 'cfcalc'
    cy.visit('/cfcalc');
    // Check if url has changed
    cy.url().should('includes', 'cfcalc');
    // Check if the component title has changed.
    cy.get('.container__name')
        .should('have.text', 'Carbon Footprint Calculator');
  });

  it('Routes that works when no user is Logged In', function() {
    // visit 'login'
    cy.visit('/login');
    // Check if url has changed
    cy.url().should('includes', 'login');
    // Check if the component title has changed.
    cy.get('.container__name').should('have.text', 'Sign In');

    // visit 'register'
    cy.visit('/register');
    // Check if url has changed
    cy.url().should('includes', 'register');
    // Check if the component title has changed.
    cy.get('.container__name').should('have.text', 'Sign Up');
  });

  it('Routes that works when a user is Logged In', function() {
    // Logging In with an existing user
    const data = {
      email: 'hiber98@gmail.com',
      password: 'hiber98',
    };

    cy.visit('/login');

    cy.get('input[id=email]').type(data.email);
    cy.get('input[id=password]').type(`${data.password}{enter}`);

    // Waiting to Log In.
    cy.wait(3000);

    // // visit 'tip/:id'
    // cy.visit('/tip/4');
    // // Check if url has changed
    // cy.url().should('includes', 'tip');
    // // Check if the component title has changed.
    // cy.get('.container__name').should('have.text', 'Tips');

    // visit 'challenge/:slug'
    cy.visit('/challenge/provem');
    // Check if url has changed
    cy.url().should('includes', 'challenge');
    // Check if the component title has changed.
    cy.get('.info__data > li')
        .should('have.text', 'Create or Update a Challenge');

    // visit 'operation/:id'
    cy.visit('/operation/3');
    // Check if url has changed
    cy.url().should('includes', 'operation');
    // Check if the component title has changed.
    cy.get('.info__data > li')
        .should('have.text', 'Create or Update an Operation');

    // visit '@username'
    cy.visit('/@hiber98');
    // Check if url has changed
    cy.url().should('includes', '@');
    // Check if the component title has changed.
    cy.get('.container__name').should('have.text', 'Profile');
  });

  it('Handling error 401', function() {
    // Logging In with an existing user
    const data = {
      email: 'hiber98@gmail.com',
      password: 'hiber98',
    };

    // visit 'challenge/:slug'
    cy.visit('/challenge/provem');
    // Check if url has changed
    cy.url().should('includes', 'challenge');
    // Check if the error 401 image is rendered
    cy.get('picture > img').should('have.class', 'errors400__401');

    // visit 'operation/:id'
    cy.visit('/operation/3');
    // Check if url has changed
    cy.url().should('includes', 'operation');
    // Check if the error 401 image is rendered
    cy.get('picture > img').should('have.class', 'errors400__401');

    // visit '@username'
    cy.visit('/@hiber98');
    // Check if url has changed
    cy.url().should('includes', '@');
    // Check if the error 401 image is rendered
    cy.get('picture > img').should('have.class', 'errors400__401');

    // Logging in
    cy.visit('/login');
    cy.get('input[id=email]').type(data.email);
    cy.get('input[id=password]').type(`${data.password}{enter}`);

    // Waiting to Log In.
    cy.wait(3000);

    // visit 'login'
    cy.visit('/login');
    // Check if url has changed
    cy.url().should('includes', 'login');
    // Check if the error 401 image is rendered
    cy.get('picture > img').should('have.class', 'errors400__401');

    // visit 'register'
    cy.visit('/register');
    // Check if url has changed
    cy.url().should('includes', 'register');
    // Check if the error 401 image is rendered
    cy.get('picture > img').should('have.class', 'errors400__401');
  });

  it('Handling error 404', function() {
    // visit 'UnexistingRoute'
    cy.visit('/UnexistingRoute');
    // Check if url has changed
    cy.url().should('includes', 'UnexistingRoute');
    // Check if the error 404 image is rendered
    cy.get('picture > img').should('have.class', 'errors400');
  });
});
