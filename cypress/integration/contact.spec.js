describe('The Contact Page', function() {
  it('Successfully Contact Form.', function() {
    // Data to fill the form.
    const data = {
      email: 'hiber98@gmail.com',
      subject: 'hiber98',
      comment: 'this is a testing comment to make it works',
    };

    // visit 'contact'
    cy.visit('/contact');

    // check if url have changed
    cy.url().should('includes', 'contact');

    // It writes a valid email and no error is displayed.
    cy.get('input[id=email]').type(data.email);
    cy.get('span[id=error-email]').should('not.be.visible');

    // It writes a valid subject and no error is displayed.
    cy.get('input[id=subject]').type(data.subject);
    cy.get('span[id=error-subject]').should('not.be.visible');

    // It writes a valid comment and no error is displayed.
    // {enter} causes the form to submit
    cy.get('textarea[id=comment]').type(`${data.comment}{enter}`);
    cy.get('span[id=error-comment]').should('not.be.visible');

    // we should be redirected to /dashboard
    cy.url().should('include', '/');
  });

  it('Form errors are displayed', function() {
    // Data to fill the form.
    const wrongData = {
      email: 'dilluns',
      subject: 'i',
      comment: 'bon dia',
    };

    // visit 'contact'
    cy.visit('/contact');

    // check if url have changed
    cy.url().should('includes', 'contact');

    // It writes an invalid email
    cy.get('input[id=email]').type(wrongData.email);

    // Email form error is displayed form error is displayed and it's visible.
    cy.get('span[id=error-email]').should('have.text', 'Invalid email');
    cy.get('span[id=error-email]').should('be.visible');

    // It writes an invalid subject
    cy.get('input[id=subject]').type(wrongData.subject);

    // Subject form error is displayed form error is displayed and it's visible.
    cy.get('span[id=error-subject]')
        .should('have.text', 'Subject too short');
    cy.get('span[id=error-subject]').should('be.visible');

    // It writes an invalid comment
    cy.get('textarea[id=comment]').type(wrongData.comment);

    // Comment form error is displayed and it's visible.
    cy.get('span[id=error-comment]')
        .should('have.text', 'Comment too short');
    cy.get('span[id=error-comment]').should('be.visible');
  });
});

