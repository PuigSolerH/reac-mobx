describe('The Challenges Page', function() {
  it('It checks the Main view', function() {
    // visit 'challenges'
    cy.visit('/challenges');

    // check if url have changed
    cy.url().should('includes', 'challenges');

    cy.get('.container__name').should('have.text', 'Challenges');
  });

  it('Checks the default products, categories and the pagination', function() {
    // visit 'challenges'
    cy.visit('/challenges');

    // Check if there are 2 tabs. The global and the categorie's name
    cy.get('.container__links > *').should('have.length', 1);

    // Check how many challenges there are
    cy.get('.challenges__items > .challenge').should('have.length', 10);

    // Check if there are 2 pages.
    cy.get('.pagination > .page__item > .page__number').should(($pages) => {
      expect($pages).to.have.length(2);
      expect($pages.eq(0)).to.contain('1');
      expect($pages.eq(1)).to.contain('2');
    });
  });

  it('Challenges get filtered by the current user username', function() {
    // Logging In with an existing user
    const data = {
      email: 'hiber98@gmail.com',
      password: 'hiber98',
    };

    cy.visit('/login');

    cy.get('input[id=email]').type(data.email);
    cy.get('input[id=password]').type(`${data.password}{enter}`);

    // Waiting to Log In.
    cy.wait(3000);

    // visit 'challenges'
    cy.visit('/challenges');

    // Click on the rem tab
    cy.contains('Your Feed').click();

    // Check if url has changed
    cy.url().should('includes', 'challenges?tab=feed');

    // Check if there are 2 tabs. The global and the categorie's name
    cy.get('.container__links > *').should('have.length', 2);

    // Check if the number of categories has changed
    cy.get('.challenges__items > .challenge').should('have.length', 5);

    // Check if the number of pages has changed
    cy.get('.pagination > .page__item > .page__number')
        .should('have.length', 2);

    // Click on Global Feed tab to list all challenges.
    cy.contains('Global Feed').click();

    // Check if there are 2 tabs. The global and the categorie's name
    cy.get('.container__links > *').should('have.length', 2);

    // Check how many challenges there are
    cy.get('.challenges__items > .challenge').should('have.length', 10);

    // Check if the number of pages has changed
    cy.get('.pagination > .page__item > .page__number')
        .should('have.length', 2);
  });

  it('Deletes a challenge', function() {
    // Logging In with an existing user
    const data = {
      email: 'hiber98@gmail.com',
      password: 'hiber98',
    };

    cy.visit('/login');

    cy.get('input[id=email]').type(data.email);
    cy.get('input[id=password]').type(`${data.password}{enter}`);

    // Waiting to Log In.
    cy.wait(3000);

    // visit 'challenges'
    cy.visit('/challenges');

    expect(
        cy.get('.challenge > .challenge__info > .info__title')
            .eq(0).should('have.text', 'Quibusdam omnis in est est.')
    );

    // Click on Delete Challenge button delete the challenge.
    cy.contains('Delete Challenge').click();

    // Check if the challenge has been deleted by finding its name.
    expect(
        cy.get('.challenge > .challenge__info > .info__title')
            .eq(0).should('not.have.text', 'Quibusdam omnis in est est.')
    );
  });

  it('Updates a challenge', function() {
    // Logging In with an existing user
    const data = {
      email: 'hiber98@gmail.com',
      password: 'hiber98',
    };

    cy.visit('/login');

    cy.get('input[id=email]').type(data.email);
    cy.get('input[id=password]').type(`${data.password}{enter}`);

    // Waiting to Log In.
    cy.wait(3000);

    // visit 'challenges'
    cy.visit('/challenges');

    expect(
        cy.get('.challenge > .challenge__info > .info__title').eq(0)
            .should('have.text', 'Cypress')
    );

    // Click on Delete Challenge button delete the challenge.
    cy.contains('Edit Challenge').click();

    // Check if url has changed
    cy.url().should('includes', 'challenge/cypress');

    // It rewrites the title
    cy.get('input[id=title]').clear();
    cy.get('input[id=title]').type('Cypress Title');

    // Click the submit button
    cy.get('.field__input--button').click();

    // Waiting to Log In.
    cy.wait(3000);

    expect(
        cy.get('.challenge > .challenge__info > .info__title').eq(0)
            .should('have.text', 'Cypress Title')
    );
  });

  it('Creates a challenge', function() {
    // Logging In with an existing user
    const data = {
      email: 'hiber98@gmail.com',
      password: 'hiber98',
    };

    cy.visit('/login');

    cy.get('input[id=email]').type(data.email);
    cy.get('input[id=password]').type(`${data.password}{enter}`);

    // Waiting to Log In.
    cy.wait(3000);

    // visit 'challenges'
    cy.visit('/challenges');

    expect(
        cy.get('.challenge > .challenge__info > .info__title').eq(0)
            .should('have.text', 'Cypress')
    );

    // Click on Delete Challenge button delete the challenge.
    cy.contains('Create Challenge').click();

    // Check if url has changed
    cy.url().should('includes', 'challenge');

    // It rewrites the title
    cy.get('input[id=title]').type('Cypress Title');
    cy.get('input[id=description]').type('Cypress Title');

    // Click the submit button
    cy.get('.field__input--button').click();

    // Waiting to Log In.
    cy.wait(3000);

    expect(
        cy.get('.challenge > .challenge__info > .info__title').eq(0)
            .should('have.text', 'Cypress Title')
    );
  });
});
