describe('The Profile Page', function() {
  it('It checks the Main view (TEACHER USER)', function() {
    // Logging In with an existing user
    const data = {
      email: 'hiber98@gmail.com',
      password: 'hiber98',
    };

    cy.visit('/login');

    cy.get('input[id=email]').type(data.email);
    cy.get('input[id=password]').type(`${data.password}{enter}`);

    // Waiting to Log In.
    cy.wait(3000);

    // visit '@hiber98' (the profile)
    cy.visit('/@hiber98');

    // check if url have changed
    cy.url().should('includes', '@hiber98');

    // Check if the user is on the profile view
    cy.get('.container__name').should('have.text', 'Profile');

    // Expect to load the current challenge
    expect(cy.get('.challenge--latest').should('have.length', 1));

    // Expect to load the advices
    expect(cy.get('.messages > *').should('have.length', 3));

    // Expect to load a chart
    expect(cy.get('.chart').should('have.length', 1));

    // Waiting to load the operations.
    cy.wait(1000);

    // Expect not to load any operation because it's a teacher class user
    expect(cy.get('.challenges__items--operations  > .operation')
        .should('have.length', 0));
  });

  it('It checks the Main view (STUDENT USER)', function() {
    // Logging In with an existing user
    const data = {
      email: 'puigsolerh@gmail.com',
      password: 'puigsolerh',
    };

    cy.visit('/login');

    cy.get('input[id=email]').type(data.email);
    cy.get('input[id=password]').type(`${data.password}{enter}`);

    // Waiting to Log In.
    cy.wait(3000);

    // visit '@puigsolerh' (the profile)
    cy.visit('/@puigsolerh');

    // check if url have changed
    cy.url().should('includes', '@puigsolerh');

    // Check if the user is on the profile view
    cy.get('.container__name').should('have.text', 'Profile');

    // Expect to load the current challenge
    expect(cy.get('.challenge--latest').should('have.length', 1));

    // Expect to load the advices
    expect(cy.get('.messages > *').should('have.length', 3));

    // Expect to load a chart
    expect(cy.get('.chart').should('have.length', 1));

    // Waiting to load the operations.
    cy.wait(1000);

    // Check if the operations has been loaded
    expect(cy.get('.challenges__items > .operation').should('have.length', 10));

    // Check the number of pages
    cy.get('.pagination > .page__item > .page__number')
        .should('have.length', 2);
  });

  it('Checks the default operations, and the pagination', function() {
    // Logging In with an existing user
    const data = {
      email: 'puigsolerh@gmail.com',
      password: 'puigsolerh',
    };

    cy.visit('/login');

    cy.get('input[id=email]').type(data.email);
    cy.get('input[id=password]').type(`${data.password}{enter}`);

    // Waiting to Log In.
    cy.wait(3000);

    // visit '@puigsolerh' (the profile)
    cy.visit('/@puigsolerh');

    // check if url have changed
    cy.url().should('includes', '@puigsolerh');

    // Check if the user is on the profile view
    cy.get('.container__name').should('have.text', 'Profile');

    // Check if the operations has been loaded
    expect(cy.get('.challenges__items > .operation').should('have.length', 10));

    // Check if there are 2 pages.
    cy.get('.pagination > .page__item > .page__number').should(($pages) => {
      expect($pages).to.have.length(2);
      expect($pages.eq(0)).to.contain('1');
      expect($pages.eq(1)).to.contain('2');
    });
  });

  it('Deletes an operation', function() {
    // Logging In with an existing user
    const data = {
      email: 'puigsolerh@gmail.com',
      password: 'puigsolerh',
    };

    cy.visit('/login');

    cy.get('input[id=email]').type(data.email);
    cy.get('input[id=password]').type(`${data.password}{enter}`);

    // Waiting to Log In.
    cy.wait(3000);

    // visit '@puigsolerh' (the profile)
    cy.visit('/@puigsolerh');

    // check if url have changed
    cy.url().should('includes', '@puigsolerh');

    expect(
        cy.get('.operation > .challenge__info > .info__title')
            .eq(0).should('have.text', 'sunt')
    );

    // Click on Delete Challenge button delete the challenge.
    cy.contains('Delete Challenge').click();

    // Check if the challenge has been deleted by finding its name.
    expect(
        cy.get('.challenge > .challenge__info > .info__title')
            .eq(0).should('not.have.text', 'sunt')
    );
  });

  it('Updates an operation', function() {
    // Logging In with an existing user
    const data = {
      email: 'puigsolerh@gmail.com',
      password: 'puigsolerh',
    };

    cy.visit('/login');

    cy.get('input[id=email]').type(data.email);
    cy.get('input[id=password]').type(`${data.password}{enter}`);

    // Waiting to Log In.
    cy.wait(3000);

    // visit '@puigsolerh' (the profile)
    cy.visit('/@puigsolerh');

    // check if url have changed
    cy.url().should('includes', '@puigsolerh');

    expect(
        cy.get('.operation > .challenge__info > .info__title')
            .eq(0).should('have.text', 'sunt')
    );

    // Click on Edit Operation button to update the operation.
    cy.contains('Edit Operation').click();

    // Check if url has changed
    cy.url().should('includes', 'operation/13');

    // It selects another category
    cy.get('select[id=category]').select('rem');

    // It rewrite the quantity
    cy.get('input[id=quantity]').clear();
    cy.get('input[id=quantity]').type('4');

    // Click the submit button
    cy.get('.field__input--button').click();

    // Waiting to Log In.
    cy.wait(3000);

    expect(
        cy.get('.operation > .challenge__info > .info__title').eq(0)
            .should('have.text', 'rem')
    );
  });
});
