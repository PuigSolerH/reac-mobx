/* eslint no-invalid-this: 0*/

describe('The Register Page', function() {
  it('signs up a new user', function() {
    // Data to fill the form.
    const data = {
      username: 'cypress',
      email: 'cypress@gmail.com',
      password: 'cypress',
      classroom: 'director',
    };

    // visit 'register'
    cy.visit('/register');

    // check if url have changed
    cy.url().should('includes', 'register');

    // It writes a valid username and no error is displayed.
    cy.get('input[id=username]').type(data.username);
    cy.get('span[id=error-username]').should('not.be.visible');

    // It writes a valid email and no error is displayed.
    cy.get('input[id=email]').type(data.email);
    cy.get('span[id=error-email]').should('not.be.visible');

    // It writes a valid password and no error is displayed.
    cy.get('input[id=password]').type(data.password);
    cy.get('span[id=error-password]').should('not.be.visible');

    // It selects a classroom.
    cy.get('select[id=classroom]').select(data.classroom);

    // clicks the submit button
    cy.get('.field__input--button').click();

    // we should be redirected to /dashboard
    cy.url().should('include', '/');

    // UI should reflect this user being logged in
    cy.get('.header__menu > .menu__prodpic')
        .should('not.have.class', 'menu__prodpic');
  });

  it('Signing Up a registered user', function() {
    // Data to fill the form.
    const data = {
      username: 'hiber98',
      email: 'hiber98@gmail.com',
      password: 'hiber98',
      classroom: 'director',
    };

    // visit 'register'
    cy.visit('/register');

    // check if url have changed
    cy.url().should('includes', 'register');

    // It writes a valid username and no error is displayed.
    cy.get('input[id=username]').type(data.username);
    cy.get('span[id=error-username]').should('not.be.visible');

    // It writes a valid email and no error is displayed.
    cy.get('input[id=email]').type(data.email);
    cy.get('span[id=error-email]').should('not.be.visible');

    // It writes a valid password and no error is displayed.
    cy.get('input[id=password]').type(data.password);
    cy.get('span[id=error-password]').should('not.be.visible');

    // It selects a classroom.
    cy.get('select[id=classroom]').select(data.classroom);

    // clicks the submit button
    cy.get('.field__input--button').click();

    // we should be redirected to /dashboard
    cy.url().should('include', '/');

    // UI should reflect this user being logged in
    cy.get('.header__menu > .menu__prodpic')
        .should('not.have.class', 'menu__prodpic');
  });

  it('Form errors are displayed', function() {
    // Data to fill the form.
    const wrongData = {
      username: 'user',
      email: 'hiber98',
      password: '1234',
    };

    // visit 'register'
    cy.visit('/register');

    // check if url have changed
    cy.url().should('includes', 'register');

    // It writes an invalid username
    cy.get('input[id=username]').type(wrongData.username);

    // Username form error is displayed form error is displayed
    // and it's visible.
    cy.get('span[id=error-username]')
        .should('have.text', 'Username is too short');
    cy.get('span[id=error-username]').should('be.visible');

    // It writes an invalid email
    cy.get('input[id=email]').type(wrongData.email);

    // Email form error is displayed form error is displayed and it's visible.
    cy.get('span[id=error-email]').should('have.text', 'Email is not valid');
    cy.get('span[id=error-email]').should('be.visible');

    // It writes an invalid password
    cy.get('input[id=password]').type(wrongData.password);

    // Password form error is displayed and it's visible.
    cy.get('span[id=error-password]')
        .should('have.text', 'Password is too short');
    cy.get('span[id=error-password]').should('be.visible');
  });
});
