describe('The Login Page', function() {
  it('Successfully Log In.', function() {
    // Data to fill the form.
    const data = {
      email: 'hiber98@gmail.com',
      password: 'hiber98',
    };

    // visit 'login'
    cy.visit('/login');

    // check if url have changed
    cy.url().should('includes', 'login');

    // It writes a valid email and no error is displayed.
    cy.get('input[id=email]').type(data.email);
    cy.get('span[id=error-email]').should('not.be.visible');

    // It writes a valid password and no error is displayed.
    // {enter} causes the form to submit
    cy.get('input[id=password]').type(`${data.password}{enter}`);
    cy.get('span[id=error-password]').should('not.be.visible');

    // we should be redirected to /dashboard
    cy.url().should('include', '/');

    // UI should reflect this user being logged in
    cy.get('.header__menu > .menu__prodpic')
        .should('have.class', 'menu__prodpic');
  });

  it('Logging In with a non saved user.', function() {
    // Data to fill the form.
    const data = {
      email: 'no@deuria.anar',
      password: 'error123',
    };

    // visit 'login'
    cy.visit('/login');

    // check if url have changed
    cy.url().should('includes', 'login');

    // It writes a valid email and no error is displayed.
    cy.get('input[id=email]').type(data.email);
    cy.get('span[id=error-email]').should('not.be.visible');

    // It writes a valid password and no error is displayed.
    // {enter} causes the form to submit
    cy.get('input[id=password]').type(`${data.password}{enter}`);
    cy.get('span[id=error-password]').should('not.be.visible');

    // we should be redirected to /dashboard
    cy.url().should('include', '/');

    // UI should reflect this user being logged in
    cy.get('.header__menu > .menu__prodpic')
        .should('not.have.class', 'menu__prodpic');
  });

  it('Form errors are displayed', function() {
    // Data to fill the form.
    const wrongData = {
      email: 'hiber98',
      password: '1234',
    };

    // visit 'login'
    cy.visit('/login');

    // check if url have changed
    cy.url().should('includes', 'login');

    // It writes an invalid email
    cy.get('input[id=email]').type(wrongData.email);

    // Email form error is displayed form error is displayed and it's visible.
    cy.get('span[id=error-email]').should('have.text', 'Email is not valid');
    cy.get('span[id=error-email]').should('be.visible');

    // It writes an invalid password
    cy.get('input[id=password]').type(wrongData.password);

    // Password form error is displayed and it's visible.
    cy.get('span[id=error-password]')
        .should('have.text', 'Password is too short');
    cy.get('span[id=error-password]').should('be.visible');
  });

  it('Logs Out', function() {
    // Data to fill the form.
    const data = {
      email: 'hiber98@gmail.com',
      password: 'hiber98',
    };

    // visit 'login'
    cy.visit('/login');

    // It writes a valid data.
    cy.get('input[id=email]').type(data.email);
    cy.get('input[id=password]').type(`${data.password}{enter}`);

    // UI should reflect this user being logged in
    cy.get('.header__menu > .menu__prodpic')
        .should('have.class', 'menu__prodpic');

    // Click on Log Out header field.
    cy.contains('Log Out').click();

    // UI should reflect this user being logged in
    cy.get('.header__menu > .menu__prodpic')
        .should('not.have.class', 'menu__prodpic');
  });
});
