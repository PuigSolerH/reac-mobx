import {observable, action, reaction} from 'mobx';
import agent from '../agent';

/**
 * It manages global state or state than can be user twice (or more times).
 *
 * @class CommonStore
 */
class CommonStore {
  /**
   * Values that can change and serve as a resource to 'paint' the interface.
   *
   * @memberof CommonStore
   */
  @observable token = window.localStorage.getItem('jwt');
  @observable appLoaded = false;
  @observable categories = [];
  @observable classrooms = [];
  @observable isLoadingData = false;

  /**
   * Creates an instance of CommonStore.
   * @memberof CommonStore
   */
  constructor() {
    reaction(
        () => this.token,
        (token) => {
          if (token) {
            window.localStorage.setItem('jwt', token);
          } else {
            window.localStorage.removeItem('jwt');
          }
        }
    );
  }

  /**
   * It gets all the categories from the backend and sends the
   * result to the component.
   *
   * @return {Array}
   * @memberof CommonStore
   */
  @action loadCategories() {
    this.isLoadingData = true;
    return agent.Categories.getAll()
        .then(action(({categories}) => {
          this.categories = categories.map((t) => t.toLowerCase());
        }))
        .finally(action(() => {
          this.isLoadingData = false;
        }));
  }

  /**
   * It gets all the categories from the backend and sends the
   * result to the component.
   *
   * @return {Array}
   * @memberof CommonStore
   */
  @action loadClassrooms() {
    this.isLoadingData = true;
    return agent.Classrooms.getAll()
        .then(action(({classrooms}) => {
          this.classrooms = classrooms.map((t) => t.toLowerCase());
        }))
        .finally(action(() => {
          this.isLoadingData = false;
        }));
  }

  /**
   * It sets a new token each time a user is logged in.
   *
   * @param {String} token
   * @memberof CommonStore
   */
  @action setToken(token) {
    this.token = token;
  }

  /**
   * It sets the app to loaded.
   *
   * @memberof CommonStore
   */
  @action setAppLoaded() {
    this.appLoaded = true;
  }

  /**
   * It thanges the styles depending on which mode has choosen the user.
   * The mode is stored on Local Storage.
   *
   * @memberof CommonStore
   */
  @action setMode() {
    window.localStorage.setItem('mode',
      (window.localStorage.getItem('mode')) === 'dark' ? 'light' : 'dark');

    window.localStorage.getItem('mode') === 'dark' ?
      document.querySelector('body').classList.add('dark')
      : document.querySelector('body').classList.remove('dark');
  }
}

export default new CommonStore();
