import {observable, action} from 'mobx';
import agent from '../agent';

const LIMIT = 10;

/**
 * It manages the 'operations' store and their actions.
 *
 * @export
 * @class OperationsStore
 */
export class OperationsStore {
  /**
   * Values that can change and serve as a resource to 'paint' the interface.
   *
   * @memberof OperationsStore
   */
  @observable isLoading = false;
  @observable inProgress = false;
  @observable operations = [];
  @observable operationsSum = [];
  @observable page = 0;
  @observable totalPagesCount = 0;
  @observable values = {
    category: '',
    quantity: '',
  };
  @observable valuesSum = {
    rem: 0,
    voluptatem: 0,
    dolor: 0,
    inventore: 0,
    sunt: 0,
  };
  @observable result = 0;

  /**
   * It updates the store with the form data.
   *
   * @param {String} param
   * @param {String} value
   * @memberof OperationsStore
   */
  @action setData(param, value) {
    this.values[param] = value;
  }

  /**
   * It updates the store with the form data.
   *
   * @param {String} param
   * @param {String} value
   * @memberof OperationsStore
   */
  @action setCalcData(param, value) {
    this.valuesSum[param] = value;
  }

  /**
   * It cleans the store data.
   * It's used when user changes to another view.
   *
   * @memberof OperationsStore
   */
  @action reset() {
    this.values.category = '';
    this.values.quantity = '';
    this.valuesSum = {
      rem: 0,
      voluptatem: 0,
      dolor: 0,
      inventore: 0,
      sunt: 0,
    };
    this.result = 0;
  }

  /**
   * It obtines the data of an operation by its id.
   *
   * @param {integer} id
   * @return {Object}
   * @memberof OperationsStore
   */
  @action loadInitialData(id) {
    this.inProgress = true;
    return this.loadOperation(id)
        .then(action((operation) => {
          if (!operation) throw new Error('Can\'t load original operation');
          this.values.category = operation.category;
          this.values.quantity = operation.quantity;
        }))
        .finally(action(() => {
          this.inProgress = false;
        }));
  }

  /**
   * It changes the current page to load another operations.
   *
   * @param {Number} page
   * @memberof OperationsStore
   */
  @action setPage(page) {
    this.page = page;
  }

  /**
   * It gets all the operations from the api server
   * filtered by the author username.
   *
   * @param {String} user
   * @return {Array}
   * @memberof OperationsStore
   */
  @action loadOperations(user) {
    this.isLoading = true;
    return agent.Operations.byAuthor(user.username, this.page, LIMIT)
        .then(action(({operations, operationsCount}) => {
          this.operations = operations;
          this.totalPagesCount = Math.ceil(operationsCount / LIMIT);
        }))
        .finally(action(() => {
          this.isLoading = false;
        }));
  }

  /**
   * It loads the specified operation by its slug.
   *
   * @param {String} id
   * @return {Object}
   * @memberof OperationsStore
   */
  @action loadOperation(id) {
    this.isLoading = true;
    return agent.Operations.get(id)
        .then(action(({operation}) => {
          return operation;
        }))
        .finally(action(() => {
          this.isLoading = false;
        }));
  }

  /**
   * It gets the amount for each category.
   *
   * @param {Object} params
   * @return {Array}
   * @memberof OperationsStore
   */
  @action loadOperationsSum(params) {
    this.isLoading = true;
    if (params) {
      return (
      (params.rol === 'student') ?
        agent.Operations.byAuthorAmount(params.username) :
        agent.Operations.byClassroomAmount(params.classroom)
      )
          .then(action((response) => {
            this.operationsSum = response;
          }))
          .finally(action(() => {
            this.isLoading = false;
          }));
    } else {
      return agent.Operations.allAmount()
          .then(action((response) => {
            this.operationsSum = response;
          }))
          .finally(action(() => {
            this.isLoading = false;
          }));
    }
  }

  /**
   * It fills the calc store with the amount of each category.
   *
   * @param {Object} params
   * @return {Array}
   * @memberof OperationsStore
   */
  @action loadCalcData(params) {
    return (
    (params.rol === 'student') ?
      agent.Operations.byAuthorAmount(params.username) :
      agent.Operations.byClassroomAmount(params.classroom)
    )
        .then(action((response) => {
          response.map((data) => {
            return this.valuesSum[data.name] = data.sum;
          });
        }))
        .finally(action(() => {
          this.isLoading = false;
        }));
  }

  /**
   * Its sends the data to the bachend to create a new operation.
   *
   * @param {Object} operation
   * @return {Object}
   * @memberof OperationsStore
   */
  @action createOperation(operation) {
    return agent.Operations.create(operation)
        .then(({operation}) => {
          return operation;
        });
  }

  /**
   * It updates an operation by its slug.
   *
   * @param {Object} operation
   * @return {Object}
   * @memberof OperationsStore
   */
  @action updateOperation(operation) {
    return agent.Operations.update(operation)
        .then(({operation}) => {
          return operation;
        });
  }

  /**
   * It deletes an operation by its id.
   *
   * @param {String} id
   * @return {Array}
   * @memberof OperationsStore
   */
  @action deleteOperation(id) {
    return agent.Operations.delete(id)
        .catch(action((err) => {
          throw err;
        }));
  }

  /**
   * It submits the data and, depending if the slug exists or not
   * the operation is created or updated.
   *
   * @param {String} id
   * @return {Response}
   * @memberof OperationsStore
   */
  @action submit(id) {
    this.inProgress = true;
    this.errors = undefined;
    const operation = {
      id: id,
      category: this.values.category,
      quantity: this.values.quantity,
    };
    return (
      id ?
      this.updateOperation(operation) :
      this.createOperation(operation)
    )
        .catch(action((err) => {
          this.errors = err.response &&
          err.response.body &&
          err.response.body.errors;
          throw err;
        }))
        .finally(action(() => {
          this.inProgress = false;
        }));
  }

  /**
   * It calculates the Carbon Footprint of the data put on the calc.
   *
   * @return {Integer}
   * @memberof OperationsStore
   */
  @action calculate() {
    const that = this;
    that.result = 0;

    Object.keys(this.valuesSum).map(function(category, index) {
      const value = parseFloat(that.valuesSum[category], 10);
      switch (category) {
        case 'rem':
        case 'plastic':
          return that.result = that.result + (value * 3.5);
          // break;

        case 'voluptatem':
        case 'paper':
          return that.result = that.result + (value * 1.2);
          // break;

        case 'dolor':
        case 'glass':
          return that.result = that.result + (value * 4.9);
          // break;

        case 'inventore':
        case 'metal':
          return that.result = that.result + (value * 4.2);
          // break;

        case 'sunt':
        case 'oil':
          return that.result = that.result + (value * 3.3);
          // break;

        case 'battery':
          return that.result = that.result + (value * 4555.4);
          // break;

        default:
          return that.result = 0;
          // break;
      }
    });

    return this.result;
  }
}

export default new OperationsStore();
