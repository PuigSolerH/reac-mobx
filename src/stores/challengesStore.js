import {observable, action, computed} from 'mobx';
import agent from '../agent';

const LIMIT = 10;

/**
 * It manages the 'challenges' store and their actions.
 *
 * @export
 * @class ChallengesStore
 */
export class ChallengesStore {
  /**
   * Values that can change and serve as a resource to 'paint' the interface.
   *
   * @memberof ChallengesStore
   */
  @observable isLoading = false;
  @observable inProgress = false;
  @observable page = 0;
  @observable totalPagesCount = 0;
  @observable challengesRegistry = observable.map();
  @observable predicate = {};
  @observable challengeSlug = undefined;
  @observable values = {
    title: '',
    description: '',
  };
  @observable latestChallenge = {};

  /**
   * It creates an slug by using a param.
   *
   * @param {String} challengeSlug
   * @memberof ChallengesStore
   */
  @action setChallengeSlug(challengeSlug) {
    if (this.challengeSlug !== challengeSlug) {
      this.reset();
      this.challengeSlug = challengeSlug;
    }
  }

  /**
   * It updates the store with the form data.
   *
   * @param {String} param
   * @param {String} value
   * @memberof ChallengesStore
   */
  @action setData(param, value) {
    this.values[param] = value;
  }

  /**
   * It cleans the store data.
   * It's used when user changes to another view.
   *
   * @memberof ChallengesStore
   */
  @action reset() {
    this.values.title = '';
    this.values.description = '';
  }

  /**
   * It obtines the data of a challenge by its slug.
   *
   * @return {Object}
   * @memberof ChallengesStore
   */
  @action loadInitialData() {
    if (!this.challengeSlug) return Promise.resolve();
    this.inProgress = true;
    return this.loadChallenge(this.challengeSlug, {acceptCached: true})
        .then(action((challenge) => {
          if (!challenge) throw new Error('Can\'t load original challenge');
          this.values.title = challenge.title;
          this.values.description = challenge.description;
        }))
        .finally(action(() => {
          this.inProgress = false;
        }));
  }

  /**
   * It gets all the challenges.
   *
   * @readonly
   * @memberof ChallengesStore
   */
  @computed get challenges() {
    return this.challengesRegistry.values();
  };


  /**
   * It clears the challenges registry.
   *
   * @memberof ChallengesStore
   */
  @action clear() {
    this.challengesRegistry.clear();
    this.page = 0;
  }

  /**
   * It changes the current page to load another challenges.
   *
   * @param {Number} page
   * @memberof ChallengesStore
   */
  @action setPage(page) {
    this.page = page;
  }

  /**
   * It takes one item as input and returns either true or false based on
   * whether the item satisfies some condition.
   *
   * @param {Boolean} predicate
   * @memberof ChallengesStore
   */
  @action setPredicate(predicate) {
    if (JSON.stringify(predicate) === JSON.stringify(this.predicate)) return;
    this.clear();
    this.predicate = predicate;
  }

  /**
   * Depending of the view that the user has choosen
   * it loads one tips or another ones.
   *
   * @return {Array}
   * @memberof ChallengesStore
   */
  $req() {
    if (this.predicate.myFeed) return agent.Challenges.feed(this.page, LIMIT);
    if (this.predicate.author) {
      return agent.Challenges.byAuthor(this.predicate.author, this.page, LIMIT);
    }
    return agent.Challenges.all(this.page, LIMIT, this.predicate);
  }

  /**
   * It loads al the challenges and pages it.
   *
   * @return {Array}
   * @memberof ChallengesStore
   */
  @action loadChallenges() {
    this.isLoading = true;
    return this.$req()
        .then(action(({challenges, challengesCount}) => {
          this.challengesRegistry.clear();
          challenges.forEach((challenge) => {
            this.challengesRegistry.set(challenge.slug, challenge);
          });
          this.totalPagesCount = Math.ceil(challengesCount / LIMIT);
        }))
        .finally(action(() => {
          this.isLoading = false;
        }));
  }

  /**
   * It loads the specified challenge by its slug.
   *
   * @param {String} slug
   * @param {Boolean} {acceptCached = false} = {}
   * @return {Object}
   * @memberof ChallengesStore
   */
  @action loadChallenge(slug, {acceptCached = false} = {}) {
    if (acceptCached) {
      const challenge = this.getChallenge(slug);
      if (challenge) return Promise.resolve(challenge);
    }
    this.isLoading = true;
    return agent.Challenges.get(slug)
        .then(action(({challenge}) => {
          this.challengesRegistry.set(challenge.slug, challenge);
          return challenge;
        }))
        .finally(action(() => {
          this.isLoading = false;
        }));
  }

  /**
   * It gets a challenge by its slug.
   *
   * @param {String} slug
   * @return {Object}
   * @memberof ChallengesStore
   */
  @action getChallenge(slug) {
    return this.challengesRegistry.get(slug);
  }

  /**
   * It gets the latest challenge by its classroom owner.
   *
   * @param {String} classroom
   * @return {Object}
   * @memberof ChallengesStore
   */
  @action getLatestChallenge(classroom) {
    this.isLoading = true;
    return agent.Challenges.byClassroom(classroom)
        .then(action(({challenges}) => {
          return this.latestChallenge = challenges[0];
        }))
        .finally(action(() => {
          this.isLoading = false;
        }));
  }

  /**
   * Its sends the data to the bachend to create a new challenge.
   *
   * @param {Object} challenge
   * @return {Object}
   * @memberof ChallengesStore
   */
  @action createChallenge(challenge) {
    return agent.Challenges.create(challenge)
        .then(({challenge}) => {
          this.challengesRegistry.set(challenge.slug, challenge);
          return challenge;
        });
  }

  /**
   * It deletes a challenge by its slug.
   *
   * @param {String} slug
   * @return {Array}
   * @memberof ChallengesStore
   */
  @action deleteChallenge(slug) {
    this.challengesRegistry.delete(slug);
    return agent.Challenges.delete(slug)
        .catch(action((err) => {
          this.loadChallenges(); throw err;
        }));
  }

  /**
   * It updates a challenge by its slug.
   *
   * @param {Object} challenge
   * @return {Object}
   * @memberof ChallengesStore
   */
  @action updateChallenge(challenge) {
    return agent.Challenges.update(challenge)
        .then(({challenge}) => {
          this.challengesRegistry.set(challenge.slug, challenge);
          return challenge;
        });
  }

  /**
   * It submits the data and, depending if the slug exists or not
   * the challenge is created or updated.
   *
   * @return {Response}
   * @memberof ChallengesStore
   */
  @action submit() {
    this.inProgress = true;
    this.errors = undefined;
    const challenge = {
      title: this.values.title,
      description: this.values.description,
      slug: this.challengeSlug,
    };
    return (
      this.challengeSlug ?
      this.updateChallenge(challenge) :
      this.createChallenge(challenge)
    )
        .catch(action((err) => {
          this.errors = err.response &&
          err.response.body &&
          err.response.body.errors;
          throw err;
        }))
        .finally(action(() => {
          this.inProgress = false;
        }));
  }
}

export default new ChallengesStore();
