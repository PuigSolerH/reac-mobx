import {observable, action} from 'mobx';
import agent from '../agent';
import toastr from 'toastr';

const TOASTR_OPTIONS = {
  closeButton: true,
  preventDuplicated: true,
  positionClass: 'toast-top-left',
};
toastr.options = TOASTR_OPTIONS;

/**
 * It manages the contact form data.
 *
 * @class ContactStore
 */
class ContactStore {
  /**
   * Values that can change and serve as a resource to 'paint' the interface.
   *
   * @memberof ContactStore
   */
  @observable inProgress = true;
  @observable errorEmail = undefined;
  @observable errorSubject = undefined;
  @observable errorComment = undefined;
  @observable values = {
    email: '',
    subject: '',
    comment: '',
  };

  /**
   * It validates the data for each form field and it returns an error
   * depending on each one fails.
   *
   * @param {String} param
   * @param {String} data
   * @memberof ContactStore
   */
  @action validateData(param, data) {
    if (param === 'email') {
      if (!data.match(
          // eslint-disable-next-line max-len
          /^([a-zA-Z0-9_\-.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/
      )) {
        this.errorEmail = true;
      } else {
        this.errorEmail = false;
      }
    } else if (param === 'subject') {
      if (data.length < 4) {
        this.errorSubject = true;
      } else {
        this.errorSubject = false;
      }
    } else if (param === 'comment') {
      if (data.length < 20) {
        this.errorComment = true;
      } else {
        this.errorComment = false;
      }
    }

    if (this.errorEmail || this.errorSubject ||
      this.errorComment || !this.values.email ||
      !this.values.subject || !this.values.comment) this.inProgress = true;
    else this.inProgress = false;
  }

  /**
   * It updates the store with the form data.
   *
   * @param {String} param
   * @param {String} data
   * @memberof ContactStore
   */
  @action setFormData(param, data) {
    this.values[param] = data;
  }

  /**
   * It cleans the store data.
   * It's used when user changes to another view.
   *
   * @memberof ContactStore
   */
  @action reset() {
    this.values.email = '';
    this.values.subject = '';
    this.values.comment = '';
  }

  /**
   * It send the form data to the backend and it returns a response
   * depending if it was success or not.
   *
   * @return {Response}
   * @memberof ContactStore
   */
  @action send() {
    this.inProgress = true;
    if (this.errorEmail || this.errorSubject || this.errorComment ||
      !this.values.email || !this.values.subject || !this.values.comment) {
      toastr['error']('Error!');
    } else {
      return agent.Contact.contact(
          this.values.email, this.values.subject, this.values.comment
      )
          .catch(action((err) => {
            toastr['error']('Error sending the mail');
          }))
          .finally(action(() => {
            toastr['success']('Email Sent!');
            this.inProgress = false;
          }));
    }
  }
}

export default new ContactStore();
