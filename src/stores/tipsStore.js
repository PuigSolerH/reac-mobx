import {observable, action, computed} from 'mobx';
import agent from '../agent';

const LIMIT = 10;

/**
 * It manages the 'tips' state and their actions.
 *
 * @export
 * @class TipsStore
 */
export class TipsStore {
  /**
   * Values that can change and serve as a resource to 'paint' the interface.
   *
   * @memberof TipsStore
   */
  @observable isLoading = false;
  @observable page = 0;
  @observable totalPagesCount = 0;
  @observable tipsRegistry = observable.map();
  @observable predicate = {};

  /**
   * Value that can be derived from the existing state or other computed values.
   *
   * @readonly
   * @memberof TipsStore
   */
  @computed get tips() {
    return this.tipsRegistry.values();
  };

  /**
   * It clears the store. It's used when the user changes to another view.
   *
   * @memberof TipsStore
   */
  clear() {
    this.tipsRegistry.clear();
    this.page = 0;
  }

  /**
   * It gets the scpecific 'tip' by its slug.
   *
   * @param {String} slug
   * @return {Object}
   * @memberof TipsStore
   */
  getTip(slug) {
    return this.tipsRegistry.get(slug);
  }

  /**
   * It changes the current page to load another tips.
   *
   * @param {Number} page
   * @memberof TipsStore
   */
  @action setPage(page) {
    this.page = page;
  }

  /**
   * It takes one item as input and returns either true or false based on
   * whether the item satisfies some condition.
   *
   * @param {Boolean} predicate
   * @memberof TipsStore
   */
  @action setPredicate(predicate) {
    if (JSON.stringify(predicate) === JSON.stringify(this.predicate)) return;
    this.clear();
    this.predicate = predicate;
  }

  /**
   * Depending of the view that the user has choosen
   * it loads one tips or another ones.
   *
   * @return {Array}
   * @memberof TipsStore
   */
  $req() {
    if (this.predicate.myFeed) return agent.Tips.feed(this.page, LIMIT);
    if (this.predicate.category) {
      return agent.Tips.byCategory(this.predicate.category, this.page, LIMIT);
    }
    if (this.predicate.author) {
      return agent.Tips.byAuthor(this.predicate.author, this.page, LIMIT);
    }
    return agent.Tips.all(this.page, LIMIT, this.predicate);
  }

  /**
   *
   *
   * @return {Array}
   * @memberof TipsStore
   */
  @action loadTips() {
    this.isLoading = true;
    return this.$req()
        .then(action(({tips, tipsCount}) => {
          this.tipsRegistry.clear();
          tips.forEach((tip) => this.tipsRegistry.set(tip.slug, tip));
          this.totalPagesCount = Math.ceil(tipsCount / LIMIT);
        }))
        .finally(action(() => {
          this.isLoading = false;
        }));
  }

  /**
   * It loads the specified tip by its slug.
   *
   * @param {String} slug
   * @param {Boolean} {acceptCached = false} = {}
   * @return {Object}
   * @memberof TipsStore
   */
  @action loadTip(slug, {acceptCached = false} = {}) {
    if (acceptCached) {
      const tip = this.getTip(slug);
      if (tip) return Promise.resolve(tip);
    }
    this.isLoading = true;
    return agent.Tips.get(slug)
        .then(action(({tip}) => {
          this.tipsRegistry.set(tip.slug, tip);
          return tip;
        }))
        .finally(action(() => {
          this.isLoading = false;
        }));
  }
}

export default new TipsStore();
