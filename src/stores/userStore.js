import {observable, action} from 'mobx';
import agent from '../agent';

/**
 * It manages logged users info and state.
 *
 * @class UserStore
 */
class UserStore {
  /**
   * Values that can change and serve as a resource to 'paint' the interface.
   *
   * @memberof UserStore
   */
  @observable currentUser;
  @observable loadingUser;

  /**
   * It obtines the data of the current user.
   *
   * @return {Object}
   * @memberof UserStore
   */
  @action pullUser() {
    this.loadingUser = true;
    return agent.Auth.current()
        .then(action(({user}) => {
          this.currentUser = user;
        }))
        .finally(action(() => {
          this.loadingUser = false;
        }));
  }

  /**
   * It cleans the current user's info state. It's used to Log Out.
   *
   * @memberof UserStore
   */
  @action forgetUser() {
    this.currentUser = undefined;
  }
}

export default new UserStore();
