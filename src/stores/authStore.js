import {observable, action} from 'mobx';
import agent from '../agent';
import userStore from './userStore';
import commonStore from './commonStore';
import toastr from 'toastr';

const TOASTR_OPTIONS = {
  closeButton: true,
  preventDuplicated: true,
  positionClass: 'toast-top-left',
};
toastr.options = TOASTR_OPTIONS;

/**
 * It manages the auth state and actions.
 *
 * @class AuthStore
 */
class AuthStore {
  /**
   * Values that can change and serve as a resource to 'paint' the interface.
   *
   * @memberof AuthStore
   */
  @observable inProgress = false;
  @observable errorUsername = undefined;
  @observable errorEmail = undefined;
  @observable errorPassword = undefined;
  @observable errorToken = undefined;
  @observable values = {
    username: '',
    email: '',
    password: '',
    classroom: '',
    token: '',
  };
  @observable errors = {
    errorUsername: '',
    errorEmail: '',
    errorPassword: '',
    errorToken: '',
  }

  /**
   * It validates the data for each form field and it returns an error
   * depending on each one fails.
   *
   * @param {String} param
   * @param {String} data
   * @memberof AuthStore
   */
  @action validateData(param, data) {
    if (param === 'username') {
      if (data.length < 6) {
        this.errorUsername = true;
        this.errors.errorUsername = 'Username is too short';
      } else {
        this.errorUsername = false;
      }
    } else if (param === 'email') {
      if (!data.match(
          // eslint-disable-next-line max-len
          /^([a-zA-Z0-9_\-.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/
      )) {
        this.errorEmail = true;
        this.errors.errorEmail = 'Email is not valid';
      } else {
        this.errorEmail = false;
      }
    } else if (param === 'password') {
      if (data.length < 6) {
        this.errorPassword = true;
        this.errors.errorPassword = 'Password is too short';
      } else {
        this.errorPassword = false;
      }
    } else if (param === 'token') {
      if (data.length !== 4) {
        this.errorToken = true;
        this.errors.errorToken = 'Token is not valid. Ex: aaaa';
      } else {
        this.errorToken = false;
      }
    }
  }

  /**
   * It updates the store with the form data.
   *
   * @param {string} param
   * @param {string} data
   * @memberof AuthStore
   */
  @action setData(param, data) {
    this.values[param] = data;
  }

  /**
   * It cleans the store data.
   * It's used when user changes to another view.
   *
   * @memberof AuthStore
   */
  @action reset() {
    this.values.username = '';
    this.values.email = '';
    this.values.password = '';
    this.values.classroom = '';
    this.values.token = '';
    this.errors.erorUsername = '';
    this.errors.errorEmail = '';
    this.errors.errorPassword = '';
    this.errors.errorToken = '';
  }

  /**
   * It sends the data to the backend for checking if
   * a user exists. If it exits the server returns the
   * user, if not, it returns an error.
   *
   * @return {Object}
   * @memberof AuthStore
   */
  @action login() {
    this.inProgress = true;
    return agent.Auth.login(this.values.email, this.values.password)
        .then(({user}) => commonStore.setToken(user.token))
        .then(() => userStore.pullUser())
        .catch(action((err) => {
          const error = Object.keys(err.response.body.errors)
            +' '+Object.values(err.response.body.errors);

          toastr['error'](error);
        }))
        .finally(action(() => {
          this.inProgress = false;
        }));
  }

  /**
   * It sends the data to the backend for checking if
   * a user exists. If it doesn't exit it creates it
   * and returns the data, if it exists, it returns an error.
   *
   * @return {Object}
   * @memberof AuthStore
   */
  @action register() {
    this.inProgress = true;
    return agent.Auth.register(
        this.values.username,
        this.values.email,
        this.values.password,
        this.values.classroom
    )
        .then(({user}) => commonStore.setToken(user.token))
        .then(() => userStore.pullUser())
        .catch(action(() => {
          toastr['error']('Username or email already exists');
        // this.history.push('/register');
        }))
        .finally(action(() => {
          this.inProgress = false;
        }));
  }

  /**
   * It cleans the store.
   *
   * @return {Promise}
   * @memberof AuthStore
   */
  @action logout() {
    commonStore.setToken(undefined);
    userStore.forgetUser();
    return Promise.resolve();
  }

  /**
   * It sends an email to the user with a token.
   *
   * @return {Response}
   * @memberof AuthStore
   */
  @action sendMail() {
    this.inProgress = true;
    return agent.Auth.sendMail(this.values.email)
        .catch(action(() => {
          toastr['error']('Error sending the mail');
        }))
        .finally(action(() => {
        // toastr['success']('Email Sent!');
          this.inProgress = false;
        }));
  }

  /**
   * It changes the user's password.
   *
   * /////////////////////////////////////////
   * IT IS NOT FINIHED YET
   * /////////////////////////////////////////
   *
   * @return {Response}
   * @memberof AuthStore
   */
  @action checkToken() {
    this.inProgress = true;
    return agent.Auth.checkToken(this.values.token)
        .then((response) => {
        (response.error) ?
          toastr['error'](response.error) :
          toastr['success'](response.message);
        })
        // .catch(action((err) => {
        //   console.log(err.response);
        //   this.errors = err.response &&
        //   err.response.body &&
        //   err.response.body.errors;
        //   // toastr['error']('Error sending the mail');
        //   throw err;
        // }))
        .finally(action(() => {
        // toastr['success']('Email Sent!');
          this.inProgress = false;
        }));
  }
}

export default new AuthStore();
