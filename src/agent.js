import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';
import commonStore from './stores/commonStore';
import authStore from './stores/authStore';

const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT = 'https://hiber-laravel.herokuapp.com/api';
// const API_ROOT = 'http://localhost:8000/api';

const encode = encodeURIComponent;

const handleErrors = (err) => {
  if (err && err.response && err.response.status === 401) {
    authStore.logout();
  }
  return err;
};

const responseBody = (res) => res.body;

const tokenPlugin = (req) => {
  if (commonStore.token) {
    req.set('authorization', `Token ${commonStore.token}`);
  }
};

const requests = {
  del: (url) =>
    superagent
        .del(`${API_ROOT}${url}`)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
  get: (url) =>
    superagent
        .get(`${API_ROOT}${url}`)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
  put: (url, body) =>
    superagent
        .put(`${API_ROOT}${url}`, body)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
  post: (url, body) =>
    superagent
        .post(`${API_ROOT}${url}`, body)
        .use(tokenPlugin)
        .end(handleErrors)
        .then(responseBody),
};

const Auth = {
  current: () =>
    requests.get('/user'),
  login: (email, password) =>
    requests.post('/users/login', {user: {email, password}}),
  register: (username, email, password, classroom) =>
    requests.post('/users', {user: {username, email, password, classroom}}),
  save: (user) =>
    requests.put('/user', {user}),
  logout: () =>
    requests.get('users/logout'),
  sendMail: (email) =>
    requests.post('/verify', {data: {email}}),
  checkToken: (token) =>
    requests.post('/updatePass/' + token, {data: {token}}),
};

const Categories = {
  getAll: () => requests.get('/categories'),
};

const Classrooms = {
  getAll: () => requests.get('/classrooms'),
};

const limit = (count, p) => `limit=${count}&offset=${p ? p * count : 0}`;

const Tips = {
  all: (page, lim = 10) =>
    requests.get(`/tips?${limit(lim, page)}`),
  byAuthor: (author, page, query) =>
    requests.get(`/tips?author=${encode(author)}&${limit(5, page)}`),
  byCategory: (category, page, lim = 10) =>
    requests.get(`/tips?category=${encode(category)}&${limit(lim, page)}`),
  feed: () =>
    requests.get('/tips/feed?limit=10&offset=0'),
  get: (slug) =>
    requests.get(`/tips/${slug}`),
};

const Challenges = {
  all: (page, lim = 10) =>
    requests.get(`/challenges?${limit(lim, page)}`),
  byAuthor: (author, page, query) =>
    requests.get(`/challenges?author=${encode(author)}&${limit(5, page)}`),
  byClassroom: (classroom) =>
    requests.get(`/challenges?classroom=${encode(classroom)}`),
  feed: () =>
    requests.get('/challenges/feed?limit=10&offset=0'),
  delete: (slug) =>
    requests.del(`/challenges/${slug}`),
  update: (challenge) =>
    requests.put(`/challenges/${challenge.slug}`, {challenge}),
  create: (challenge) =>
    requests.post('/challenges', {challenge}),
  get: (slug) =>
    requests.get(`/challenges/${slug}`),
};

const Contact = {
  contact: (email, subject, comment) =>
    requests.post('/contact', {data: {email, subject, comment}}),
};

const Operations = {
  all: (page, lim = 10) =>
    requests.get(`/operations?${limit(lim, page)}`),
  allAmount: () =>
    requests.get(`/operationsAmount`),
  byAuthor: (student, page, lim = 10) =>
    requests.get(`/operations?student=${encode(student)}&${limit(lim, page)}`),
  byAuthorAmount: (student) =>
    requests.get(`/operationsAmount?student=${encode(student)}`),
  byClassroomAmount: (classroom) =>
    requests.get(`/operationsAmount?classroom=${encode(classroom)}`),
  delete: (id) =>
    requests.del(`/operations/${id}`),
  update: (operation) =>
    requests.put(`/operations/${operation.id}`, {operation}),
  create: (operation) =>
    requests.post('/operations', {operation}),
  get: (id) =>
    requests.get(`/operations/${id}`),
};

export default {
  Auth,
  Contact,
  Categories,
  Classrooms,
  Tips,
  Challenges,
  Operations,
};
