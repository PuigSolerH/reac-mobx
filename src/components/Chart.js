import React from 'react';
import {Bar} from 'react-chartjs-2';
import LoadingSpinner from './LoadingSpinner';
import NoContent from './Errors/NoContent';

/**
 * It lists the chart which contains the amount
 * of the operations for each category,
 *
 * @param {Object} props
 * @return {JSX}
 */
const Chart = (props) => {
  const operationsCategory = [];
  const operationsValues = [];
  props.columns.map((columns) => {
    operationsCategory.push(columns.name);
    operationsValues.push(columns.sum);
  });
  const data = {
    labels: operationsCategory,
    datasets: [
      {
        label: 'Quantity',
        backgroundColor: 'rgba(255,99,132,0.2)',
        borderColor: 'rgba(255,99,132,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
        hoverBorderColor: 'rgba(255,99,132,1)',
        data: operationsValues,
      },
    ],
  };

  if (props.loading && props.columns.length === 0) {
    return (
      <LoadingSpinner />
    );
  }

  if (props.columns.length === 0) {
    return (
      <NoContent />
    );
  }

  return (
    <section className="chart">
      <Bar
        data={data}
        width={100}
        height={50}
        options={{
          maintainAspectRatio: true,
        }}
      />
    </section>
  );
};

export default Chart;
