import React from 'react';

/**
 * It returns the banner which is used on the 'home' view.
 *
 * @param {*} props
 * @return {JSX}
 */
const Banner = () => {
  return (
    <main>
      <p className="title">GROEN MENNTUN</p>
      <article className="banner">
        <img
          className="banner_image"
          src="https://pixabay.com/get/52e1dc414c53b108f5d08460825668204022dfe05656784a762c7edd/compost-419261_1280.jpg"
          alt=""
        />
        <img
          className="banner_image"
          src="https://pixabay.com/get/57e0d1414e53ad14f6da8c7dda6d49214b6ac3e456547640762c7fd797/alternative-energy-1042411_1280.jpg"
          alt=""
        />
        <img
          className="banner_image"
          src="https://pixabay.com/get/55e1d0424856a914f6d1867dda6d49214b6ac3e456547640762c7edc97/beer-bottles-3151245_1920.jpg"
          alt=""
        />
        <img
          className="banner_image"
          src="https://pixabay.com/get/55e3d3454f54b108feda8460825668204022dfe05656784a762879dc/fence-336656_1920.jpg"
          alt=""
        />
        <img
          className="banner_image"
          src="https://pixabay.com/get/52e1d7404c5baa14f6d1867dda6d49214b6ac3e456547640762c7cd393/ecological-footprint-4123696_1920.jpg"
          alt=""
        />
      </article>
    </main>
  );
};

export default Banner;


