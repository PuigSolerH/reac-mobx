import Chart from '../Chart';
import React from 'react';
import {inject, observer} from 'mobx-react';
import {withRouter} from 'react-router-dom';
import Banner from './Banner';
/* eslint no-invalid-this: 0*/

/**
 * It loads the home view depending of the tab which is the user on.
 *
 * @class Home
 * @extends {React.Component}
 */
@inject('operationsStore')
@withRouter
@observer
class Home extends React.Component {
  /**
   * When the component has been loaded it pulls the user data if it exists.
   *
   * @memberof Home
   */
  componentDidMount() {
    this.props.operationsStore.loadOperationsSum();
  }

  /**
   * It renders the view.
   *
   * @return {JSX}
   * @memberof Home
   */
  render() {
    const {operationsSum, isLoading} = this.props.operationsStore;

    return (
      <main className="container">
        <h1 className="container__name">Home</h1>
        <section className="container__module container__module--tips">
          <Banner />
          <p className="total-recycled">TOTAL RECYCLED</p>
          <Chart columns={operationsSum} loading={isLoading} />
        </section>
      </main>
    );
  }
}

export default Home;
