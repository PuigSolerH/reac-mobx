import React from 'react';

/**
 * Image which is displayed when an anonymous user try to acceed to
 * an authorized URL.
 *
 * @class NoContent
 * @extends {React.Component}
 */
class Error401 extends React.Component {
  /**
   * It renders the view
   *
   * @return {JSX}
   * @memberof Error401
   */
  render() {
    return (
      <picture>
        <source
          media="(max-width: 750px)"
          srcSet={'../images/errors/error401-min.jpg'}
          className="errors400 errors400__401"
          alt="Error 401. Unauthorized"
        />
        <img
          className="errors400 errors400__401"
          src={'../images/errors/error401.jpg'}
          alt="Error 401. Unauthorized"
        />
      </picture>
    );
  }
}

export default Error401;
