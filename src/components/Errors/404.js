import React from 'react';

/**
 * Image which is displayed when the user try to acceed to a non valid URL.
 *
 * @class NoContent
 * @extends {React.Component}
 */
class Error404 extends React.Component {
  /**
   * It renders the view
   *
   * @return {JSX}
   * @memberof Error404
   */
  render() {
    return (
      <picture>
        <source
          media="(max-width: 750px)"
          srcSet={'../images/errors/error404-min.png'}
          className="errors400"
          alt="Error 404. Dead link"
        />
        <img
          className="errors400"
          src={'../images/errors/error404.png'}
          alt="Error 404. Dead link"
        />
      </picture>
    );
  }
}

export default Error404;
