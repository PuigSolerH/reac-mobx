import React from 'react';

/**
 * Image which is displayed when no content is loaded.
 *
 * @class NoContent
 * @extends {React.Component}
 */
class NoContent extends React.Component {
  /**
   * It renders the view
   *
   * @return {JSX}
   * @memberof NoContent
   */
  render() {
    return (
      <img
        className='noContent'
        src={'../images/errors/noContent.png'}
        alt='no content available'
      />
    );
  }
}

export default NoContent;
