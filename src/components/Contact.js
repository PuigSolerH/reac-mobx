import {withRouter} from 'react-router-dom';
import React from 'react';
import {inject, observer} from 'mobx-react';
/* eslint no-invalid-this: 0*/

/**
 * It prints the contact form
 *
 * @class Contact
 * @extends {React.Component}
 */
@inject('contactStore')
@withRouter
@observer
class Contact extends React.Component {
  /**
   * When the component is unloaded it cleans the store.
   *
   * @memberof Contact
   */
  componentWillUnmount() {
    this.props.contactStore.reset();
  }

  /**
   * It stores the data and validates it field by field.
   *
   * @param {Object} e
   * @memberof Contact
   */
  handleData = (e) => {
    this.props.contactStore.setFormData(e.target.id, e.target.value);
    this.props.contactStore.validateData(e.target.id, e.target.value);
  };


  /**
   * It submits the form data.
   *
   * @param {Object} e
   * @memberof Contact
   */
  handleSubmitForm = (e) => {
    e.preventDefault();
    this.props.contactStore.send()
        .then(() => this.props.history.replace('/'));
  };

  /**
   * It renders the form.
   *
   * @return {JSX}
   * @memberof Contact
   */
  render() {
    const {
      values,
      inProgress,
      errorEmail,
      errorSubject,
      errorComment,
    } = this.props.contactStore;

    return (
      <section className="container container--form">
        <h1 className="container__name">Contact Form</h1>
        <article className="container__module">
          <aside className="module__info">
            <h2>Groen Menntun</h2>
            <ul className="info__data">
              <li>C/ Calle 10</li>
              <li>96 777 777 1</li>
              <li>test@test.es</li>
            </ul>
          </aside>
          <form className="container__form" onSubmit={this.handleSubmitForm}>
            <span className="form__field">
              <label htmlFor="email" className="field__label">
                Email (obligatorio):
              </label>
              <input
                className="field__input"
                type="email"
                id="email"
                placeholder="Email"
                value={values.email}
                onChange={this.handleData} />
              <span
                id="error-email"
                hidden={!errorEmail}
                style={{color: 'red'}}
              >
                Invalid email
              </span>
              <p id="ayuda" required aria-describedby="ayuda">
                Ejemplo: test@gmail.com
              </p>
            </span>
            <span className="form__field">
              <label htmlFor="subject" className="field__label">
                Subject (obligatorio):
              </label>
              <input
                className="field__input"
                type="text"
                id="subject"
                placeholder="Subject"
                value={values.subject}
                onChange={this.handleData} />
              <span
                id="error-subject"
                hidden={!errorSubject}
                style={{color: 'red'}}
              >
                Subject too short
              </span>
            </span>
            <span className="form__field--full">
              <label htmlFor="comment" className="field__label">
                Comment (obligatorio):
              </label>
              <textarea
                className="field__input"
                name="comment"
                rows="5"
                id="comment"
                placeholder="Comment"
                value={values.comment}
                onChange={this.handleData}>
              </textarea>
              <span
                id="error-comment"
                hidden={!errorComment}
                style={{color: 'red'}}
              >
                Comment too short
              </span>
            </span>
            <span className="form__field--full">
              <button
                className="field__input field__input--button"
                type="submit"
                aria-label="Enviar formulario"
                disabled={inProgress}>
                Submit
              </button>
            </span>
          </form>
        </article>
      </section>
    );
  }
}

export default Contact;
