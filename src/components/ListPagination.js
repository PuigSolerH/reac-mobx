import React from 'react';

/**
 * It creates pages depending of the items amount.
 *
 * @param {Object} props
 * @return {JSX}
 */
const ListPagination = (props) => {
  if (props.totalPagesCount < 2) {
    return null;
  }

  const range = [];
  for (let i = 0; i < props.totalPagesCount; ++i) {
    range.push(i);
  }

  return (
    <ul className="pagination">
      {
        range.map((v) => {
          const isCurrent = v === props.currentPage;
          const onClick = (ev) => {
            ev.preventDefault();
            props.onSetPage(v);
          };
          return (
            <li
              className={
                isCurrent ? 'page__item page__item--selected' : 'page__item'
              }
              onClick={onClick}
              key={v.toString()}
            >
              <a className="page__number" href="">{v + 1}</a>
            </li>
          );
        })
      }
    </ul>
  );
};

export default ListPagination;
