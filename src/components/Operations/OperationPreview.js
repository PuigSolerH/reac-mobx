import React from 'react';
import {inject, observer} from 'mobx-react';
import OperationActions from './OperationActions';
/* eslint no-invalid-this: 0*/

/**
 * It prints the preview of each Operation.
 *
 * @class OperationPreview
 * @extends {React.Component}
 */
@inject('operationsStore', 'userStore')
@observer
class OperationPreview extends React.Component {
  /**
   * It manages the deleting process.
   * @param {String} id
   * @memberof OperationPreview
   */
  handleDeleteOperation = (id) => {
    this.props.operationsStore.deleteOperation(id)
        .then(() => window.location.reload());
  };

  /**
   * It renders the view.
   *
   * @return {JSX}
   * @memberof OperationPreview
   */
  render() {
    const {operation} = this.props;
    const {currentUser} = this.props.userStore;
    const canModify = currentUser && currentUser.username ===
    operation.author.username;

    return (
      <article className="operation">
        <OperationActions
          canModify={canModify}
          operation={operation}
          onDelete={this.handleDeleteOperation}
        />
        <span className="challenge__info">
          <p className="info__title">{operation.category}</p>
          <p className="info__descr">{operation.quantity}</p>
        </span>
      </article>
    );
  }
}

export default OperationPreview;
