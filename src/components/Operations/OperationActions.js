import {Link} from 'react-router-dom';
import React from 'react';

/**
 * It loads the actions that a user can do with the operation
 * if some requirements are met.
 *
 * @param {Object} props
 * @return {JSX}
 */
const OperationActions = (props) => {
  const operation = props.operation;

  /**
   * It deletes a operation by its id.
   * @return {none}
   */
  const handleDelete = () => props.onDelete(operation.id);

  if (props.canModify) {
    return (
      <span>
        <Link
          to={`/operation/${operation.id}`}
          className="challenge__button challenge__button--edit entypo-pencil"
        >
          <i className="ion-edit" /> Edit Operation
        </Link>
        <button
          className="challenge__button challenge__button--delete entypo-trash"
          onClick={handleDelete}
        >
          <i className="ion-trash-a" /> Delete Operation
        </button>
      </span>
    );
  } else return null;
};

export default OperationActions;
