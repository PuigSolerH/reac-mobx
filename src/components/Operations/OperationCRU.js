import React from 'react';
import {inject, observer} from 'mobx-react';
import {withRouter} from 'react-router-dom';
/* eslint no-invalid-this: 0*/

/**
 * Component which manage the Create and Update Operation functions.
 *
 * @class OperationCRU
 * @extends {React.Component}
 */
@inject('operationsStore', 'commonStore', 'userStore')
@withRouter
@observer
class OperationCRU extends React.Component {
  /**
   * Once the component is loaded it pulls the initial data.
   *
   * @memberof OperationCRU
   */
  componentDidMount() {
    this.props.operationsStore.loadInitialData(this.props.match.params.id);
    this.props.commonStore.loadCategories();
  }

  /**
   * When the component is updated it load a new props.
   *
   * @param {Object} prevProps
   * @memberof OperationCRU
   */
  componentDidUpdate(prevProps) {
    if (this.props.match.params.id !== prevProps.match.params.id) {
      this.props.operationsStore.loadInitialData(this.props.match.params.id);
    }
  }

  /**
   * It changes the store with the new data.
   *
   * @param {Object} e
   * @memberof OperationCRU
   */
  changeData = (e) => {
    this.props.operationsStore.setData(e.target.id, e.target.value);
  }

  /**
   * It submits the data.
   *
   * @param {Object} ev
   * @memberof OperationCRU
   */
  submitForm = (ev) => {
    ev.preventDefault();
    const {operationsStore, userStore} = this.props;
    operationsStore.submit(this.props.match.params.id)
        .then(() => {
          operationsStore.reset();
          this.props.history.replace(`/@${userStore.currentUser.username}`);
        });
  };

  /**
   * It renders the view.
   *
   * @return {JSX}
   * @memberof OperationCRU
   */
  render() {
    const {inProgress, values} = this.props.operationsStore;
    const {categories} = this.props.commonStore;

    return (
      <section className="container container--form">
        <h1 className="container__name">Operations</h1>
        <article className="container__module">
          <aside className="module__info">
            <h2>Operations</h2>
            <ul className="info__data">
              <li>Create or Update an Operation</li>
            </ul>
          </aside>
          <form className="container__form" onSubmit={this.handleSubmitForm}>
            <span className="form__field">
              <label htmlFor="category" className="field__label">
              Category (obligatorio):
              </label>
              <select
                onChange={this.changeData}
                disabled={inProgress}
                id="category"
                className="field__input"
                value={values.category}
              >
                {
                  categories.map((category) => {
                    return (
                      <option value={category} key={category}>
                        {category}
                      </option>
                    );
                  })
                }
              </select>
            </span>
            <span className="form__field">
              <label htmlFor="description" className="field__label">
              Quantity (obligatorio):
              </label>
              <input
                className="field__input"
                type="number"
                min="1"
                id="quantity"
                placeholder="Quantity"
                value={values.quantity}
                onChange={this.changeData}
                disabled={inProgress}
              />
            </span>
            <span className="form__field--full">
              <button
                className="field__input field__input--button"
                type="button"
                disabled={inProgress}
                onClick={this.submitForm}
              >
                Publish Operation
              </button>
            </span>
          </form>
        </article>
      </section>
    );
  }
}

export default OperationCRU;
