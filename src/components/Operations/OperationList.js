import OperationPreview from './OperationPreview';
import ListPagination from '../ListPagination';
import React from 'react';
import {Link} from 'react-router-dom';
import LoadingSpinner from '../LoadingSpinner';
import NoContent from '../Errors/NoContent';

/**
 * It lists the 'operations view' which contains the operations,
 * the option to create Operations and the paginations if it's needed.
 *
 * @param {Object} props
 * @return {JSX}
 */
const OperationList = (props) => {
  if (props.loading && props.operations.length === 0) {
    return (
      <LoadingSpinner />
    );
  }

  if (props.operations.length === 0) {
    return (
      <article className="news-container">
        <section className="challenges__items">
          <NoContent />
          {(!props.currentUser || props.currentUser.rol !== 'teacher') ? null :
            <Link
              to={`/challenge/`}
              className="challenge__button
                challenge__button--create
                entypo-plus-circled"
            >
              Create Operation
            </Link>
          }
        </section>
      </article>
    );
  }

  return (
    <article className="challenges__container">
      {(props.currentUser.rol !== 'student') ?
        null : (
          <Link
            to={`/operation/`}
            className="challenge__button
              challenge__button--create
              entypo-plus-circled"
          >
            Create Operation
          </Link>
        )
      }
      <section className="challenges__items challenges__items--operations">
        {
          props.operations.map((operation) => {
            return (
              <OperationPreview operation={operation} key={operation.id} />
            );
          })
        }
      </section>
      <ListPagination
        onSetPage={props.onSetPage}
        totalPagesCount={props.totalPagesCount}
        currentPage={props.currentPage}
      />
    </article>
  );
};

export default OperationList;
