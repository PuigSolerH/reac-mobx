import React from 'react';
import {Link} from 'react-router-dom';
import {inject, observer} from 'mobx-react';


/**
 * Depending if there is a user logged or not
 * it loads one menu fields or another ones.
 *
 * @param {Object} props
 * @return {JSX}
 */
const Menu = (props) => {
  return (
    <header className="header" aria-label="Menú principal">
      <input className="header__checkbox" type="checkbox" id="checkbox1" />
      <label htmlFor="checkbox1">
        <span className="header__toggle" aria-label="Menú responsive">
          Menu
        </span>
        <ul className="header__menu header__menu--main" role="menu">
          <li>
            <Link to="/" className="menu__link--logo" alt="Logo"></Link>
          </li>
          <button className="entypo-adjust" onClick={props.screenMode}></button>
          <li>
            <Link to="/tips" className="menu__link">Tips</Link>
          </li>
          <li>
            <Link to="/challenges" className="menu__link">Challenges</Link>
          </li>
          <li>
            <Link to="/cfcalc" className="menu__link">
              Carbon Footprint Calculator
            </Link>
          </li>
          <li>
            <Link to="/contact" className="menu__link">Contact</Link>
          </li>
          <li>
            <Link to="/downloads" className="menu__link">Downloads</Link>
          </li>
          {!props.currentUser ? (
            <span className="header__menu">
              <li>
                <Link to="/login" className="menu__link">Sign in</Link>
              </li>
              <li>
                <Link to="/register" className="menu__link">Sign up</Link>
              </li>
            </span>
          ) : (
              <span className="header__menu">
                <img
                  src={props.currentUser.image}
                  className="menu__prodpic" alt=""
                />
                <li>
                  <Link
                    to={`/@${props.currentUser.username}`}
                    className="menu__link"
                  >
                    {props.currentUser.username}
                  </Link>
                </li>
                <li>
                  <Link to="/" className="menu__link" onClick={props.logout}>
                    Log Out
                  </Link>
                </li>
              </span>
            )}
        </ul>
      </label>
    </header>
  );
};

/**
 * Draw top menu navbar
 *
 * @class Header
 * @extends {React.Component}
 */
@inject('userStore', 'commonStore', 'authStore')
@observer
class Header extends React.Component {
  /**
   * It renders the template and returns it.
   *
   * @return {JSX}
   * @memberof Header
   */
  render() {
    return (
      <nav className="navbar navbar-light">
        <div className="container">
          <Menu
            currentUser={this.props.userStore.currentUser}
            logout={this.props.authStore.logout}
            screenMode={this.props.commonStore.setMode}
          />
        </div>
      </nav>
    );
  }
}

export default Header;
