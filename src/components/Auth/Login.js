import {withRouter, Link} from 'react-router-dom';
import React from 'react';
import {inject, observer} from 'mobx-react';
/* eslint no-invalid-this: 0*/

/**
 * It prints the login form.
 *
 * @class Login
 * @extends {React.Component}
 */
@inject('authStore')
@withRouter
@observer
class Login extends React.Component {
  /**
   * When the component is unloaded it cleans the store.
   *
   * @memberof Login
   */
  componentWillUnmount() {
    this.props.authStore.reset();
  }

  /**
   * It stores the data and validates it field by field.
   *
   * @param {Object} e
   * @memberof Login
   */
  handleData = (e) => {
    this.props.authStore.setData(e.target.id, e.target.value);
    this.props.authStore.validateData(e.target.id, e.target.value);
  };

  /**
   * It submits the form data.
   *
   * @param {Object} e
   * @memberof Login
   */
  handleSubmitForm = (e) => {
    e.preventDefault();
    this.props.authStore.login()
        .then(() => this.props.history.replace('/'));
  };

  /**
   * It renders the form.
   *
   * @return {JSX}
   * @memberof Login
   */
  render() {
    const {
      values,
      errors,
      inProgress,
      errorEmail,
      errorPassword,
    } = this.props.authStore;

    return (
      <section className="container container--form">
        <h1 className="container__name">Sign In</h1>
        <article className="container__module">
          <aside className="module__info">
            <h2>Groen Menntun</h2>
            <ul className="info__data">
              <li><Link to="register">Need an account?</Link></li>
            </ul>
          </aside>
          <form className="container__form" onSubmit={this.handleSubmitForm}>
            <span className="form__field">
              <label htmlFor="email" className="field__label">
                Email (obligatorio):
              </label>
              <input
                className="field__input"
                type="email"
                id="email"
                placeholder="Email"
                value={values.email}
                onChange={this.handleData} />
              <span
                id="error-email"
                hidden={!errorEmail}
                style={{color: 'red'}}
              >
                {errors.errorEmail}
              </span>
              <p id="ayuda" required aria-describedby="ayuda">
                Ejemplo: test@gmail.com
              </p>
            </span>
            <span className="form__field">
              <label htmlFor="password" className="field__label">
                Password (obligatorio):
              </label>
              <input
                className="field__input"
                type="password"
                id="password"
                placeholder="Password"
                value={values.password}
                onChange={this.handleData} />
              <span
                id="error-password"
                hidden={!errorPassword}
                style={{color: 'red'}}
              >
                {errors.errorPassword}
              </span>
            </span>
            <span className="form__field--full">
              <button
                className="field__input field__input--button"
                type="submit"
                disabled={inProgress}
              >
                Sign in
              </button>
            </span>
            <p className="text-xs-center">
              <Link to="verify">Did you forget your password?</Link>
            </p>
          </form>
        </article>
      </section>
    );
  }
}

export default Login;

