import React from 'react';
import {inject, observer} from 'mobx-react';
/* eslint no-invalid-this: 0*/

/**
 * It prints the form to write a registed email
 * to proceed to change the password.
 *
 * @class SendMailForgotPassword
 * @extends {React.Component}
 */
@inject('authStore')
@observer
class SendMailForgotPassword extends React.Component {
  /**
   * When the component is unloaded it cleans the store.
   *
   * @memberof SendMailForgotPassword
   */
  componentWillUnmount() {
    this.props.authStore.reset();
  }

  /**
   * It stores the data and validates it field by field.
   *
   * @param {Object} e
   * @memberof SendMailForgotPassword
   */
  handleData = (e) => {
    this.props.authStore.setData(e.target.id, e.target.value);
    this.props.authStore.validateData(e.target.id, e.target.value);
  };

  /**
   * It submits the form data.
   *
   * @param {Object} e
   * @memberof SendMailForgotPassword
   */
  handleSubmitForm = (e) => {
    e.preventDefault();
    this.props.authStore.sendMail()
        .then(() => this.props.history.replace('/'));
  };

  /**
   * It renders the view.
   *
   * @return {JSX}
   * @memberof SendMailForgotPassword
   */
  render() {
    const {values, errors, inProgress, errorEmail} = this.props.authStore;

    return (
      <div className="auth-page">
        <div className="container page">
          <div className="row">

            <div className="col-md-6 offset-md-3 col-xs-12">
              <h1 className="text-xs-center">Fill the fields</h1>
              <p className="text-xs-center">
                Write your email and to recieve a mail to change your password.
              </p>

              <form onSubmit={this.handleSubmitForm}>
                <fieldset>

                  <fieldset className="form-group">
                    <input
                      className="form-control form-control-lg"
                      type="email"
                      id="email"
                      placeholder="Email"
                      value={values.email}
                      onChange={this.handleData}
                    />
                    <span hidden={!errorEmail} style={{color: 'red'}}>
                      {errors.errorEmail}
                    </span>
                  </fieldset>

                  <button
                    className="btn btn-lg btn-primary pull-xs-right"
                    type="submit"
                    disabled={inProgress}
                  >
                    Send Email
                  </button>

                </fieldset>
              </form>
            </div>

          </div>
        </div>
      </div>
    );
  }
}

export default SendMailForgotPassword;

