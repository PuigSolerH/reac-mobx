import {Link} from 'react-router-dom';
import React from 'react';
import {inject, observer} from 'mobx-react';
/* eslint no-invalid-this: 0*/

/**
 * It prints the register form.
 *
 * @class Register
 * @extends {React.Component}
 */
@inject('authStore', 'commonStore')
@observer
class Register extends React.Component {
  /**
   * Once the component is loaded it pulls the initial data.
   *
   * @memberof Register
   */
  componentDidMount() {
    this.props.commonStore.loadClassrooms();
  }

  /**
   * When the component is unloaded it cleans the store.
   *
   * @memberof Register
   */
  componentWillUnmount() {
    this.props.authStore.reset();
  }

  /**
   * It stores the data and validates it field by field.
   *
   * @param {Object} e
   * @memberof Register
   */
  handleData = (e) => {
    this.props.authStore.setData(e.target.id, e.target.value);
    this.props.authStore.validateData(e.target.id, e.target.value);
  };

  /**
   * It submits the form data.
   *
   * @param {Object} e
   * @memberof Register
   */
  handleSubmitForm = (e) => {
    e.preventDefault();
    this.props.authStore.register()
        .then(() => this.props.history.replace('/'));
  };

  /**
   * It renders the form.
   *
   * @return {JSX}
   * @memberof Register
   */
  render() {
    const {
      values,
      errors,
      inProgress,
      errorUsername,
      errorEmail,
      errorPassword,
    } = this.props.authStore;
    const {classrooms} = this.props.commonStore;

    return (
      <section className="container container--form">
        <h1 className="container__name">Sign Up</h1>
        <article className="container__module">
          <aside className="module__info">
            <h2>Groen Menntun</h2>
            <ul className="info__data">
              <li><Link to="login">Have an account?</Link></li>
            </ul>
          </aside>
          <form className="container__form" onSubmit={this.handleSubmitForm}>
            <span className="form__field">
              <label htmlFor="username" className="field__label">
                Username (obligatorio):
              </label>
              <input
                className="field__input"
                type="username"
                id="username"
                placeholder="Username"
                value={values.username}
                onChange={this.handleData} />
              <span
                id="error-username"
                hidden={!errorUsername}
                style={{color: 'red'}}
              >
                {errors.errorUsername}
              </span>
            </span>
            <span className="form__field">
              <label htmlFor="email" className="field__label">
                Email (obligatorio):
              </label>
              <input
                className="field__input"
                type="email"
                id="email"
                placeholder="Email"
                value={values.email}
                onChange={this.handleData} />
              <span
                id="error-email"
                hidden={!errorEmail}
                style={{color: 'red'}}
              >
                {errors.errorEmail}
              </span>
              <p id="ayuda" required aria-describedby="ayuda">
                Ejemplo: test@gmail.com
              </p>
            </span>
            <span className="form__field">
              <label htmlFor="password" className="field__label">
                Password (obligatorio):
              </label>
              <input
                className="field__input"
                type="password"
                id="password"
                placeholder="Password"
                value={values.password}
                onChange={this.handleData} />
              <span
                id="error-password"
                hidden={!errorPassword}
                style={{color: 'red'}}
              >
                {errors.errorPassword}
              </span>
            </span>
            <span className="form__field">
              <label htmlFor="classroom" className="field__label">
                Classroom (obligatorio):
              </label>
              <select
                onChange={this.handleData}
                disabled={inProgress}
                id="classroom"
                className="field__input"
                value={values.classroom}
              >
                <option value="">-------------------------------------</option>
                {
                  classrooms.map((classroom) => {
                    return (
                      <option value={classroom} key={classroom}>
                        {classroom}
                      </option>
                    );
                  })
                }
              </select>
            </span>
            <span className="form__field--full">
              <button
                className="field__input field__input--button"
                type="submit"
                disabled={inProgress}
              >
                Sign up
              </button>
            </span>
          </form>
        </article>
      </section>
    );
  }
}

export default Register;

