import React from 'react';
import {inject, observer} from 'mobx-react';
import {withRouter} from 'react-router-dom';
import OperationList from '../Operations/OperationList';
import Chart from '../Chart';
import LimitAlert from '../LimitAlert';
import LatestChallenge from '../Challenges/LatestChallenge';
import Calculator from '../CFCalculator/Calculator';
/* eslint no-invalid-this: 0*/

/**
 * It loads the profile view.
 *
 * @class Profile
 * @extends {React.Component}
 */
@inject('operationsStore', 'userStore', 'challengesStore')
@withRouter
@observer
class Profile extends React.Component {
  /**
   * When the component has been loaded it pulls the data from the server.
   *
   * @memberof Profile
   */
  componentDidMount() {
    this.props.operationsStore.loadOperationsSum(
        this.props.userStore.currentUser
    );

    this.props.operationsStore.loadOperations(
        this.props.userStore.currentUser
    );

    this.props.challengesStore.getLatestChallenge(
        this.props.userStore.currentUser.classroom
    );

    this.props.operationsStore.loadCalcData(
        this.props.userStore.currentUser
    );
  }

  /**
   * It manages the pages.
   *
   * @param {String} page
   * @memberof Operations
   */
  handleSetPage = (page) => {
    this.props.operationsStore.setPage(page);
    this.props.operationsStore.loadOperations(this.props.userStore.currentUser);
  };

  /**
   * It renders the view.
   *
   * @return {JSX}
   * @memberof Profile
   */
  render() {
    const {
      operations,
      operationsSum,
      isLoading,
      totalPagesCount,
      page,
    } = this.props.operationsStore;
    const {currentUser} = this.props.userStore;
    const {latestChallenge} = this.props.challengesStore;

    return (
      <main className="container container--profile">
        <h1 className="container__name">Profile</h1>
        <section>
          <LatestChallenge
            challenge={latestChallenge}
            key={latestChallenge.title}
          />
          <LimitAlert columns={operationsSum} />
          <Chart columns={operationsSum} loading={isLoading} />
          <Calculator />
          {
            (
              this.props.userStore.currentUser.rol === 'student'
            ) ? (
              <OperationList
                currentUser={currentUser}
                operations={operations}
                loading={isLoading}
                totalPagesCount={totalPagesCount}
                onSetPage={this.handleSetPage}
                currentPage={page}
              />
            ) : (
              null
            )
          }
        </section>
      </main>
    );
  }
}

export default Profile;
