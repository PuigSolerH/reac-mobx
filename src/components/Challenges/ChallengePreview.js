import React from 'react';
import {inject, observer} from 'mobx-react';
import ChallengeMeta from './ChallengeMeta';
/* eslint no-invalid-this: 0*/

/**
 * It prints the preview of each Challenge.
 *
 * @class ChallengePreview
 * @extends {React.Component}
 */
@inject('challengesStore', 'userStore')
@observer
class ChallengePreview extends React.Component {
  /**
   * It manages the deleting process.
   * @param {String} slug
   * @memberof ChallengePreview
   */
  handleDeleteChallenge = (slug) => {
    this.props.challengesStore.deleteChallenge(slug);
    // .then(() => window.location.reload());
  };

  /**
   * It renders the view.
   *
   * @return {JSX}
   * @memberof ChallengePreview
   */
  render() {
    const {challenge} = this.props;
    const {currentUser} = this.props.userStore;
    const canModify = currentUser && currentUser.username ===
      challenge.author.username;

    return (
      <article className="challenge">
        <span className="challenge__info">
          <p className="info__title">{challenge.title}</p>
          <p className="info__descr">{challenge.description}</p>
        </span>
        <ChallengeMeta
          challenge={challenge}
          canModify={canModify}
          onDelete={this.handleDeleteChallenge}
        />
      </article>
    );
  }
}

export default ChallengePreview;
