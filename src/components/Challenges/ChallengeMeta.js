import ChallengeActions from './ChallengeActions';
import React from 'react';
import {observer} from 'mobx-react';

/**
 * It loads the Challenge's author info which the options that
 * the owner can do with that challenge.
 *
 * @param {Object} props
 * @return {JSX}
 */
const ChallengeMeta = observer((props) => {
  const challenge = props.challenge;
  return (
    <section className="challenge__meta">
      <p>
        {challenge.author.username} ({challenge.author.classroom})
        {new Date(challenge.updatedAt).toDateString()}
      </p>
      {challenge.author.rol === 'teacher' ? (
        <ChallengeActions
          canModify={props.canModify}
          challenge={challenge}
          onDelete={props.onDelete}
        />
      ) : (
          null
        )}
    </section>
  );
});

export default ChallengeMeta;
