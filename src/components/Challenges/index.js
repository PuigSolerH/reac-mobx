import ChallengeList from './ChallengeList';
import React from 'react';
import {inject, observer} from 'mobx-react';
import {withRouter, NavLink} from 'react-router-dom';
import {parse as qsParse} from 'query-string';
/* eslint no-invalid-this: 0*/

/**
 * Current user tab.
 *
 * @param {Object} props
 * @return {JSX}
 */
const YourFeedTab = (props) => {
  if (props.currentUser) {
    return (
      <NavLink
        className="links__link"
        isActive={
          (match, location) => {
            return location.search.match('tab=feed') ? 1 : 0;
          }
        }
        to={{
          pathname: '/challenges',
          search: '?tab=feed',
        }}
      >
          Your Feed
      </NavLink>
    );
  }
  return null;
};

/**
 * Global tab.
 *
 * @param {Object} props
 * @return {JSX}
 */
const GlobalFeedTab = (props) => {
  return (
    <NavLink
      className="links__link"
      isActive={
        (match, location) => {
          return !location.search.match(/tab=(feed)/) ? 1 : 0;
        }
      }
      to={{
        pathname: '/challenges',
        search: '?tab=all',
      }}
    >
        Global Feed
    </NavLink>
  );
};

/**
 * It loads the challenges dpending of the tab which is the user on.
 *
 * @class MainView
 * @extends {React.Component}
 */
@inject('challengesStore', 'commonStore', 'userStore')
@withRouter
@observer
class MainView extends React.Component {
  /**
   *
   *
   * @memberof MainView
   */
  componentWillMount() {
    this.props.challengesStore.setPredicate(this.getPredicate());
  }

  /**
   * Once the component is loaded it pulls the challenges.
   *
   * @memberof MainView
   */
  componentDidMount() {
    this.props.challengesStore.loadChallenges();
  }

  /**
   * When there is a change on the component it loads the new props.
   *
   * @param {Object} previousProps
   * @memberof MainView
   */
  componentDidUpdate(previousProps) {
    if (this.getTab(this.props) !== this.getTab(previousProps)) {
      this.props.challengesStore.setPredicate(this.getPredicate());
      this.props.challengesStore.loadChallenges();
    }
  }

  /**
   * It gets the tab.
   *
   * @param {Object} [props=this.props]
   * @return {String}
   * @memberof MainView
   */
  getTab(props = this.props) {
    return qsParse(props.location.search).tab || 'all';
  }

  /**
   * It loads one view or another one depending of
   * the user which is logged in.
   *
   * @param {Object} [props=this.props]
   * @return {Object}
   * @memberof MainView
   */
  getPredicate(props = this.props) {
    switch (this.getTab(props)) {
      case 'feed': return {author: this.props.userStore.currentUser.username};
      default: return {};
    }
  }

  /**
   * It manages the tabs.
   *
   * @param {String} tab
   * @memberof MainView
   */
  handleTabChange = (tab) => {
    if (this.props.location.query.tab === tab) return;
    this.props.router.push({...this.props.location, query: {tab}});
  };

  /**
   * It manages the pages.
   *
   * @param {String} page
   * @memberof MainView
   */
  handleSetPage = (page) => {
    this.props.challengesStore.setPage(page);
    this.props.challengesStore.loadChallenges();
  };

  /**
   * It renders the view.
   *
   * @return {JSX}
   * @memberof MainView
   */
  render() {
    const {currentUser} = this.props.userStore;
    const {
      challenges,
      isLoading,
      page,
      totalPagesCount,
    } = this.props.challengesStore;

    return (
      <main className="container">
        <h1 className="container__name">Challenges</h1>
        <section className="container__module container__module--tips">
          <aside className="container__links">
            {(currentUser && currentUser.rol === 'teacher') ?
              <YourFeedTab currentUser={currentUser} tab={this.getTab()} /> :
              null
            }
            <GlobalFeedTab tab={this.getTab()} />
          </aside>
          <ChallengeList
            user={currentUser}
            challenges={challenges}
            loading={isLoading}
            totalPagesCount={totalPagesCount}
            currentPage={page}
            onSetPage={this.handleSetPage}
          />
        </section>
      </main>
    );
  }
}

export default MainView;
