import {Link} from 'react-router-dom';
import React from 'react';

/**
 * It loads the actions that a user can do with the challenge
 * if some requirements are met.
 *
 * @param {Object} props
 * @return {JSX}
 */
const ChallengeActions = (props) => {
  const challenge = props.challenge;

  /**
   * It deletes a challenge by its slug.
   * @return {none}
   */
  const handleDelete = () => props.onDelete(challenge.slug);

  if (props.canModify) {
    return (
      <span>
        <Link
          to={`/challenge/${challenge.slug}`}
          className="challenge__button challenge__button--edit entypo-pencil"
        >
          <i className="ion-edit" /> Edit Challenge
        </Link>
        <button
          className="challenge__button challenge__button--delete entypo-trash"
          onClick={handleDelete}
        >
          <i className="ion-trash-a" /> Delete Challenge
        </button>
      </span>
    );
  } else return null;
};

export default ChallengeActions;
