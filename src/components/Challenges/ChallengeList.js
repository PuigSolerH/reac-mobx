import ChallengePreview from './ChallengePreview';
import ListPagination from '../ListPagination';
import React from 'react';
import {Link} from 'react-router-dom';
import LoadingSpinner from '../LoadingSpinner';
import NoContent from '../Errors/NoContent';

/**
 * It lists the 'challenges view' which contains the challenges,
 * the option to create Challenges and the paginations if it's needed.
 *
 * @param {Object} props
 * @return {JSX}
 */
const ChallengeList = (props) => {
  if (props.loading && props.challenges.length === 0) {
    return (
      <LoadingSpinner />
    );
  }

  if (props.challenges.length === 0) {
    return (
      <article className="news-container">
        <section className="challenges__items">
          <NoContent />
          {(!props.user || props.user.rol !== 'teacher') ? null :
            <Link
              to={`/challenge/`}
              className="challenge__button
                challenge__button--create
                entypo-plus-circled"
            >
              Create Challenge
            </Link>
          }
        </section>
      </article>
    );
  }

  return (
    <article className="challenges__container">
      {(!props.user || props.user.rol !== 'teacher') ? null :
          <Link
            to={`/challenge/`}
            className="challenge__button
              challenge__button--create
              entypo-plus-circled"
          >
            Create Challenge
          </Link>
      }
      <section className="challenges__items">
        {
          props.challenges.map((challenge) => {
            return (
              <ChallengePreview challenge={challenge} key={challenge.title} />
            );
          })
        }
      </section>
      <ListPagination
        onSetPage={props.onSetPage}
        totalPagesCount={props.totalPagesCount}
        currentPage={props.currentPage}
      />
    </article>
  );
};

export default ChallengeList;
