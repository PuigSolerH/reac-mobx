import React from 'react';
import LoadingSpinner from '../LoadingSpinner';

/**
 * It lists the challenge obtained from the request.
 * It will be the newest one filtered by the user's classroom.
 *
 * @param {Object} props
 * @return {JSX}
 */
const LatestChallenge = (props) => {
  if (!props.challenge.title) {
    return (
      <LoadingSpinner />
    );
  } else {
    return (
      <article className="challenge challenge--latest">
        <span className="challenge__info">
          <p className="info__title">{props.challenge.title}</p>
          <p className="info__descr">{props.challenge.description}</p>
        </span>
      </article>
    );
  }
};

export default LatestChallenge;
