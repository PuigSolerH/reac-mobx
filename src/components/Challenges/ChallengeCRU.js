import React from 'react';
import {inject, observer} from 'mobx-react';
import {withRouter} from 'react-router-dom';
/* eslint no-invalid-this: 0*/

/**
 * Component which manage the Create and Update Challenge functions.
 *
 * @class ChallengeCRU
 * @extends {React.Component}
 */
@inject('challengesStore')
@withRouter
@observer
class ChallengeCRU extends React.Component {
  /**
   *
   * @memberof ChallengeCRU
   */
  componentWillMount() {
    this.props.challengesStore.setChallengeSlug(this.props.match.params.slug);
  }

  /**
   * Once the component is loaded it pulls the initial data.
   *
   * @memberof ChallengeCRU
   */
  componentDidMount() {
    this.props.challengesStore.loadInitialData();
  }

  /**
   * When the component is updated it load a new props.
   *
   * @param {Object} prevProps
   * @memberof ChallengeCRU
   */
  componentDidUpdate(prevProps) {
    if (this.props.match.params.slug !== prevProps.match.params.slug) {
      this.props.challengesStore.setChallengeSlug(this.props.match.params.slug);
      this.props.challengesStore.loadInitialData();
    }
  }

  /**
   * It changes the store with the new data.
   *
   * @param {Object} e
   * @memberof ChallengeCRU
   */
  changeData = (e) => {
    this.props.challengesStore.setData(e.target.id, e.target.value);
  }

  /**
   * It submits the data.
   *
   * @param {Object} ev
   * @memberof ChallengeCRU
   */
  submitForm = (ev) => {
    ev.preventDefault();
    const {challengesStore} = this.props;
    challengesStore.submit()
        .then((challenge) => {
          challengesStore.reset();
          // this.props.history.replace(`/challenge/${challenge.slug}`);
          this.props.history.replace(`/challenges`);
        });
  };

  /**
   * It renders the view.
   *
   * @return {JSX}
   * @memberof ChallengeCRU
   */
  render() {
    const {inProgress, values} = this.props.challengesStore;

    return (
      <section className="container container--form">
        <h1 className="container__name">Challenges</h1>
        <article className="container__module">
          <aside className="module__info">
            <h2>Challenges</h2>
            <ul className="info__data">
              <li>Create or Update a Challenge</li>
            </ul>
          </aside>
          <form className="container__form" onSubmit={this.handleSubmitForm}>
            <span className="form__field">
              <label htmlFor="title" className="field__label">
              Title (obligatorio):
              </label>
              <input
                className="field__input"
                type="text"
                id="title"
                placeholder="Challenge Title"
                value={values.title}
                onChange={this.changeData}
                disabled={inProgress}
              />
            </span>
            <span className="form__field">
              <label htmlFor="description" className="field__label">
              Description (obligatorio):
              </label>
              <input
                className="field__input"
                type="text"
                id="description"
                placeholder="What's this challenge about?"
                value={values.description}
                onChange={this.changeData}
                disabled={inProgress}
              />
            </span>
            <span className="form__field--full">
              <button
                className="field__input field__input--button"
                type="button"
                disabled={inProgress}
                onClick={this.submitForm}
              >
                Publish Challenge
              </button>
            </span>
          </form>
        </article>
      </section>
    );
  }
}

export default ChallengeCRU;
