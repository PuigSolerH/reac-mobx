import React from 'react';
import {Link} from 'react-router-dom';

/**
 * It prints all the categories.
 *
 * @param {Object} props
 * @return {JSX}
 */
const Categories = (props) => {
  const categories = props.categories;
  if (categories) {
    return (
      <div className="categories">
        {
          categories.map((category) => {
            return (
              <Link
                to={{
                  pathname: '/tips',
                  search: '?tab=category&category=' + category,
                }}
                className="categories__category"
                key={category}
              >
                {category}
              </Link>
            );
          })
        }
      </div>
    );
  } else {
    return (
      <p>Cargando...</p>
    );
  }
};

export default Categories;
