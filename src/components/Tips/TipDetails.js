import React from 'react';
import {inject, observer} from 'mobx-react';
import {withRouter} from 'react-router-dom';

/**
 * Component which loads the detais of a Tip.
 *
 * @class TipDetails
 * @extends {React.Component}
 */
@inject('tipsStore', 'userStore')
@withRouter
@observer
class TipDetails extends React.Component {
  /**
   * When the component has been loaded it pulls tip by its id.
   *
   * @memberof TipDetails
   */
  componentDidMount() {
    const slug = this.props.match.params.id;
    this.props.tipsStore.loadTip(slug, {acceptCached: true});
  }

  /**
   * It renders the view.
   *
   * @return {JSX}
   * @memberof TipDetails
   */
  render() {
    const slug = this.props.match.params.id;
    const tip = this.props.tipsStore.getTip(slug);

    if (!tip) return <h3>Not tip found!</h3>;

    return (
      <section className="container">
        <article className="article">
          <span className="article__pic">
            <img className="article__image" src={tip.image} alt="" />
          </span>
          <aside className="article__meta">
            <p>{tip.category}</p>
            <p>{tip.author.username}</p>
            <p>{new Date(tip.createdAt).toDateString()}</p>
          </aside>
          <h2 className="article__title">{tip.title}</h2>
          <p className="article__header">{tip.header}</p>
          <p className="article__content">{tip.body}</p>
        </article>
      </section>
    );
  }
}

export default TipDetails;
