import TipsList from './TipsList';
import React from 'react';
import {inject, observer} from 'mobx-react';
import {withRouter, NavLink} from 'react-router-dom';
import {parse as qsParse} from 'query-string';
/* eslint no-invalid-this: 0*/

/**
 * Current user tab.
 *
 * @param {Object} props
 * @return {JSX}
 */
const YourFeedTab = (props) => {
  if (props.currentUser) {
    return (
      <NavLink
        className="links__link"
        isActive={
          (match, location) => {
            return location.search.match('tab=feed') ? 1 : 0;
          }
        }
        to={{
          pathname: '/tips',
          search: '?tab=feed',
        }}
      >
        Your Feed
      </NavLink>
    );
  }
  return null;
};

/**
 * Global tab.
 *
 * @param {Object} props
 * @return {JSX}
 */
const GlobalFeedTab = (props) => {
  return (
    <NavLink
      className="links__link"
      isActive={
        (match, location) => {
          return !location.search.match(/tab=(feed|category)/) ? 1 : 0;
        }
      }
      to={{
        pathname: '/tips',
        search: '?tab=all',
      }}
    >
        Global Feed
    </NavLink>
  );
};

/**
 * Category tab.
 *
 * @param {Object} props
 * @return {JSX}
 */
const CategoryFilterTab = (props) => {
  if (!props.category) {
    return null;
  }

  return (
    <a className="links__link">
      <i className="ion-pound" /> {props.category}
    </a>
  );
};

/**
 * It loads the tips dpending of the tab which is the user on.
 *
 * @class MainView
 * @extends {React.Component}
 */
@inject('tipsStore', 'commonStore', 'userStore')
@withRouter
@observer
class MainView extends React.Component {
  /**
   *
   *
   * @memberof MainView
   */
  componentWillMount() {
    this.props.tipsStore.setPredicate(this.getPredicate());
  }

  /**
   * Once the component is loaded it pulls the tips.
   *
   * @memberof MainView
   */
  componentDidMount() {
    this.props.tipsStore.loadTips();
  }

  /**
   * When there is a change on the component it loads the new props.
   *
   * @param {Object} previousProps
   * @memberof MainView
   */
  componentDidUpdate(previousProps) {
    if (
      this.getTab(this.props) !== this.getTab(previousProps) ||
      this.getCategory(this.props) !== this.getCategory(previousProps)
    ) {
      this.props.tipsStore.setPredicate(this.getPredicate());
      this.props.tipsStore.loadTips();
    }
  }

  /**
   * It gets the category.
   *
   * @param {Object} [props=this.props]
   * @return {String}
   * @memberof MainView
   */
  getCategory(props = this.props) {
    return qsParse(props.location.search).category || '';
  }

  /**
   * It gets the tab.
   *
   * @param {Object} [props=this.props]
   * @return {String}
   * @memberof MainView
   */
  getTab(props = this.props) {
    return qsParse(props.location.search).tab || 'all';
  }

  /**
   * It loads one view or another one depending of:
   *  · The category.
   *  · The user which is logged in.
   *
   * @param {Object} [props=this.props]
   * @return {Object}
   * @memberof MainView
   */
  getPredicate(props = this.props) {
    switch (this.getTab(props)) {
      case 'feed': return {author: this.props.userStore.currentUser.username};
      case 'category':
        return {category: qsParse(props.location.search).category};
      default: return {};
    }
  }

  /**
   * It manages the tabs.
   *
   * @param {String} tab
   * @memberof MainView
   */
  handleTabChange = (tab) => {
    if (this.props.location.query.tab === tab) return;
    this.props.router.push({...this.props.location, query: {tab}});
  };

  /**
   * It manages the pages.
   *
   * @param {String} page
   * @memberof MainView
   */
  handleSetPage = (page) => {
    this.props.tipsStore.setPage(page);
    this.props.tipsStore.loadTips();
  };

  /**
   * It renders the view.
   *
   * @return {JSX}
   * @memberof MainView
   */
  render() {
    const {currentUser} = this.props.userStore;
    const {tips, isLoading, page, totalPagesCount} = this.props.tipsStore;

    return (
      <section className="container__module container__module--tips">
        <aside className="container__links">
          <YourFeedTab currentUser={currentUser} tab={this.getTab()} />

          <GlobalFeedTab tab={this.getTab()} />

          <CategoryFilterTab
            category={qsParse(this.props.location.search).category}
          />
        </aside>
        <TipsList
          tips={tips}
          loading={isLoading}
          totalPagesCount={totalPagesCount}
          currentPage={page}
          onSetPage={this.handleSetPage}
        />
      </section>
    );
  }
}

export default MainView;

