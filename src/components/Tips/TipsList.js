import TipPreview from './TipPreview';
import ListPagination from '../ListPagination';
import React from 'react';
import LoadingSpinner from '../LoadingSpinner';
import NoContent from '../Errors/NoContent';

/**
 * It lists the 'tips view' which contains the tips,
 * and the paginations if it's needed.
 *
 * @param {Object} props
 * @return {JSX}
 */
const TipsList = (props) => {
  if (props.loading && props.tips.length === 0) {
    return (
      <LoadingSpinner />
    );
  }

  if (props.tips.length === 0) {
    return (
      <NoContent />
    );
  }

  return (
    <article className="news-container">
      <section className="news__items">
        {
          props.tips.map((tip) => {
            return (
              <TipPreview tip={tip} key={tip.slug} />
            );
          })
        }
      </section>
      <ListPagination
        onSetPage={props.onSetPage}
        totalPagesCount={props.totalPagesCount}
        currentPage={props.currentPage}
      />
    </article>
  );
};

export default TipsList;
