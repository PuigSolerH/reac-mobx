import React from 'react';
import {Link} from 'react-router-dom';
import {inject, observer} from 'mobx-react';

/**
 * Component which manage how will the tips be displayed one by one.
 *
 * @class TipPreview
 * @extends {React.Component}
 */
@inject('tipsStore')
@observer
class TipPreview extends React.Component {
  /**
   * It renders the view.
   *
   * @return {JSX}
   * @memberof TipPreview
   */
  render() {
    const {tip} = this.props;

    return (
      <Link to={`/tip/${tip.slug}`} className="news">
        <img className="news__image" src={tip.image} alt="" />
        <svg width="100%" height="50px">
          <line
            stroke="black"
            strokeWidth="2"
            x1="42"
            y1="20"
            x2="9999999999"
            y2="20">
          </line>
        </svg>
        <article className="news__info">
          <p className="info__title">{tip.title}</p>
          <p className="info__preview">{tip.description}</p>
          <p className="info__category" key={tip.category}>{tip.category}</p>
        </article>
        <span className="news__meta">
          <span className="author">
            <img className="author-image" src={tip.author.image} alt="" />
            {tip.author.username}
          </span>
          {new Date(tip.createdAt).toDateString()}
        </span>
      </Link>
    );
  }
}

export default TipPreview;
