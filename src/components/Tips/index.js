import MainView from './MainView';
import React from 'react';
import Categories from './Categories';
import {inject, observer} from 'mobx-react';
import {withRouter} from 'react-router-dom';

/**
 * It's the main view which loads the Tips and the Categories
 * all together.
 *
 * @class Tips
 * @extends {React.Component}
 */
@inject('commonStore')
@withRouter
@observer
class Tips extends React.Component {
  /**
   * When the component has been loaded it pulls the categories.
   *
   * @memberof Tips
   */
  componentDidMount() {
    this.props.commonStore.loadCategories();
  }

  /**
   * It renders the view.
   *
   * @return {JSX}
   * @memberof Tips
   */
  render() {
    const {categories} = this.props.commonStore;
    return (
      <section className="container">
        <h1 className="container__name">Tips</h1>
        <aside className="container__categories">
          <p className="categories__title">Categories</p>
          <Categories categories={categories} />
        </aside>
        <MainView />
      </section>
    );
  }
}

export default Tips;

