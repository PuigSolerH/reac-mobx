import Header from './Header';
import React from 'react';
import {Switch, Route, withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';

import Home from './Home';
import Login from './Auth/Login';
import Register from './Auth/Register';
import SendMailForgotPassword from './Auth/SendMailForgotPassword';
import SetNewPassword from './Auth/SetNewPassword';
import Contact from './Contact';
import Tips from './Tips';
import TipDetails from './Tips/TipDetails';
import Challenges from './Challenges';
import ChallengeCRU from './Challenges/ChallengeCRU';
import Profile from './Profile';
import OperationCRU from './Operations/OperationCRU';
import Download from './Download';
import Error404 from './Errors/404';
import Error401 from './Errors/401';
import CFCalculator from './CFCalculator';

/**
 * It defines the web routes.
 *
 * @class App
 * @extends {React.Component}
 */
@inject('userStore', 'commonStore')
// @inject('userStore', 'commonStore', 'challengesStore', 'operationsStore')
@withRouter
@observer
class App extends React.Component {
  /**
   * When the component has been loaded it pulls the user data if it exists.
   *
   * @memberof App
   */
  componentDidMount() {
    if (this.props.commonStore.token) {
      this.props.userStore.pullUser()
          .finally(() => this.props.commonStore.setAppLoaded());
    } else this.props.commonStore.setAppLoaded();
  }

  /**
   * It renders all the routes.
   *
   * @return {JSX}
   * @memberof App
   */
  render() {
    if (this.props.commonStore.appLoaded) {
      return (
        <div>
          <Header />
          <Switch>
            <Route path="/" exact component={Home} />
            {/* <Route path="/login" component={Login} />
            <Route path="/register" component={Register} /> */}
            <Route path="/login"
              render={() => (
                (!this.props.userStore.currentUser) ?
                  <Login /> : <Error401 />
              )}
            />
            <Route path="/register"
              render={() => (
                (!this.props.userStore.currentUser) ?
                  <Register /> : <Error401 />
              )}
            />
            <Route path="/verify" component={SendMailForgotPassword} />
            <Route path="/token" component={SetNewPassword} />
            <Route path="/contact" component={Contact} />
            <Route path="/tips" component={Tips} />
            <Route path="/tip/:id" component={TipDetails} />
            <Route path="/challenges" component={Challenges} />
            {/* <Route path="/challenge/:slug?" component={ChallengeCRU} /> */}
            <Route path="/challenge/:slug?"
              render={() => (
                (this.props.userStore.currentUser) ?
                  <ChallengeCRU /> : <Error401 />
              )}
            />
            {/* <Route path="/operation/:id?" component={OperationCRU} /> */}
            <Route path="/operation/:id?"
              render={() => (
                (this.props.userStore.currentUser) ?
                  <OperationCRU /> : <Error401 />
              )}
            />
            {/* <Route path="/@:username" component={Profile} /> */}
            <Route path="/@:username"
              render={() => (
                (this.props.userStore.currentUser) ?
                  <Profile /> : <Error401 />
              )}
            />
            <Route path="/downloads" component={Download} />
            <Route path="/cfcalc" component={CFCalculator} />
            <Route component={Error404} />
          </Switch>
        </div>
      );
    }
    return (
      <Header />
    );
  }
}

export default App;

