import React from 'react';

/**
 * Component which contains the Installers for Linux and Windows.
 *
 * @class Download
 * @extends {React.Component}
 */
class Download extends React.Component {
  /**
   * It renders the view.
   *
   * @return {JSX}
   * @memberof Download
   */
  render() {
    return (
      <section className="container">
        <h1 className="container__name">Downloads</h1>
        <div className="container--downloads">
          <article className="download">
            <span className="download__name">Linux</span>
            <img
              className="download__image"
              src={'../images/downloads/linux.png'}
              alt=""
            />
            <a className="download__button" href={'https://groenmen.tk/downloads/GroenMenntun-linux.deb'}>Download</a>
          </article>
          <article className="download">
            <span className="download__name">Windows</span>
            <img
              className="download__image"
              src={'../images/downloads/windows.png'}
              alt=""
            />
            <a className="download__button" href={'https://groenmen.tk/downloads/GroenMenntun-windows.exe'}>Download</a>
          </article>
        </div>
      </section>
    );
  }
}

export default Download;
