import React from 'react';
import {Link} from 'react-router-dom';

/**
 * It shows an alert if the user
 * has reached the limit of one material
 * or he almost did it.
 *
 * @param {Object} props
 * @return {JSX}
 */
const LimitAlert = (props) => {
  return (
    <aside className="messages">
      {
        props.columns.map((column) => {
          if (column.sum > 50 && column.sum < 70) {
            return (
              <span
                className="message message__warning entypo-attention"
                key={column.name}
              >
                Warning! You have almost reached the limit of
                <strong>
                  <Link
                    className="warning__link"
                    to={`/tips?tab=category&category=`+column.name}
                  >
                    {column.name}
                  </Link>
                </strong>
              </span>
            );
          } else if (column.sum >= 70) {
            return (
              <span
                className="message message__alert entypo-alert"
                key={column.name}
              >
                Alert! You have exceeded the limit of
                <strong>
                  <Link
                    className="alert__link"
                    to={`/tips?tab=category&category=`+column.name}
                  >
                    {column.name}
                  </Link>
                </strong>
              </span>
            );
          } else {
            return null;
          }
        })
      }
    </aside>
  );
};

export default LimitAlert;
