import React from 'react';
import {inject, observer} from 'mobx-react';
import {withRouter} from 'react-router-dom';
/* eslint no-invalid-this: 0*/

/**
 * Carbon Footprint calculator.
 *
 * @class Calculator
 * @extends {React.Component}
 */
@inject('userStore', 'operationsStore', 'commonStore')
@withRouter
@observer
class Calculator extends React.Component {
  /**
   * Once the component is loaded it pulls the initial data.
   *
   * @memberof CFCalculator
   */
  componentDidMount() {
    this.props.commonStore.loadCategories();
    this.props.operationsStore.loadOperationsSum(
        this.props.userStore.currentUser
    );

    // ////////////////////////////////////////////////////////////////////////
    if (this.props.userStore.currentUser) {
      this.calculate();
    }
    // ////////////////////////////////////////////////////////////////////////
  }

  /**
   * When the component is unloaded it cleans the store.
   *
   * @memberof Calculator
   */
  componentWillUnmount() {
    this.props.operationsStore.reset();
  }

  /**
   * It stores the data and validates it field by field.
   *
   * @param {Object} e
   * @memberof Calculator
   */
  handleData = (e) => {
    this.props.operationsStore.setCalcData(e.target.id, e.target.value);
  };

  /**
   * It calculates the Carbon Footprint using the data that user
   * has written on the inputs.
   *
   * @memberof Calculator
   */
  calculate = () => {
    this.props.operationsStore.calculate();
  }

  /**
   * It renders the view.
   *
   * @return {JSX}
   * @memberof Calculator
   */
  render() {
    const {valuesSum, result} = this.props.operationsStore;
    const {categories} = this.props.commonStore;

    return (
      <article className="calculator">
        {
          categories.map((category) => {
            return (
              <span className="calculator__field" key={category}>
                <label className="calculator__name">{category} (kg or u)</label>
                <input
                  className="calculator__input"
                  type="number"
                  min="0"
                  step="any"
                  id={category}
                  value={valuesSum[category]}
                  onChange={this.handleData}
                />
              </span>
            );
          })
        }
        <button className="calculator__btn" onClick={this.calculate}>
          CALCULATE
        </button>
        <span className="calculator__result">
          <input
            className="calculator__result calculator__result--input"
            type="text"
            readOnly
            placeholder="0"
            value={result}
          />kg/CO2
        </span>
      </article>
    );
  }
};

export default Calculator;
