import React from 'react';
import Calculator from './Calculator';
import Facts from './Facts';

/**
 * Main view of the carbon footprint calculator.
 *
 * @class CFCalculator
 * @extends {React.Component}
 */
class CFCalculator extends React.Component {
  /**
   * It renders the view.
   *
   * @return {JSX}
   * @memberof CFCalculator
   */
  render() {
    return (
      <main className="container">
        <h1 className="container__name">Carbon Footprint Calculator</h1>
        <section className="container__cfcalculator">
          <Calculator />
          <Facts />
        </section>
      </main>
    );
  }
}

export default CFCalculator;
