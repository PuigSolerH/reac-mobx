import React from 'react';

/**
 * It returns the slider which contains some comparisions about the
 * Carbon Footprint.
 *
 * @param {*} props
 * @return {JSX}
 */
const Facts = () => {
  return (
    <aside className="slider__container">
      <div className="slider">
        <span className="slider__slide">
          <h3>Did you know?</h3>
            14 kg of CO2 emitted are the equivalent of:
          <ul>
            <li>A car trip of 70 km.</li>
            <li>Five hamburgers with cheese.</li>
            <li>Nine liters of milk.</li>
            <li>6.6 minutes of a transatlantic flight.</li>
          </ul>
        </span>
        <span className="slider__slide">
          <h3>Did you know?</h3>
          <p>
            1000 searches on Google are equivalent to 200 g of CO2.
          </p>
        </span>
        <span className="slider__slide">
          <h3>Did you know?</h3>
          <p>
            Production of 1 American cheeseburger emits 3.1 kg of CO2.
          </p>
        </span>
        <span className="slider__slide">
          <h3>Did you know?</h3>
          <p>
            Reading a newspaper can consume 20% less carbon than
            watching the news online.
          </p>
        </span>
        <span className="slider__slide">
          <h3>Did you know?</h3>
            The primary sources of carbon emissions are:
          <ul>
            <li>Solid fuels such as coal - 35%</li>
            <li>Liquid fuels such as oil or petroleum - 36%</li>
            <li>Gaseous fuels such as natural gas - 20%</li>
            <li>
              Industrial gas emissions from manufacturing or
              processing plants - less than 1%
            </li>
            <li>
              Deforestation which increases emissions by decreasing the
              cleaning performed by forests - 5%
            </li>
          </ul>
        </span>
      </div>
    </aside>
  );
};

export default Facts;
