const createWindowsInstaller =
  require('electron-winstaller').createWindowsInstaller;
const path = require('path');

getInstallerConfig()
    .then(createWindowsInstaller)
    .catch((error) => {
      console.error(error.message || error);
      process.exit(1);
    });

/**
 * Sets all the config to create the installer for Windows.
 *
 * @return {Promise}
 */
function getInstallerConfig() {
  const rootPath = path.join('./');
  const outPath = path.join(rootPath, 'release-builds');
  console.log(path.join(outPath, 'GroenMenntun-win32-ia32/'));

  return Promise.resolve({
    appDirectory: path.join(outPath, 'GroenMenntun-win32-ia32'),
    authors: 'Hibernon Puig',
    description: 'Eduactional app abaout recycling.',
    noMsi: true,
    outputDirectory: path.join(outPath, 'GroenMenntun'),
    exe: 'GroenMenntun.exe',
    setupExe: 'GroenMenntun.exe',
    setupIcon: path.join(
        rootPath, 'public', 'images', 'favicons', 'favicon.ico'
    ),
    loadingGif: path.join(rootPath, 'public', 'images', 'gifs', 'loading.gif'),
  });
}
